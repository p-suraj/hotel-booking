﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using DataLayer;
using DataLayers.Domain;
using DataLayers.NhSetup;
using DataLayers.SharedUtility;
using Hotel_Booking.WebApi;
using NUnit.Framework;

namespace RepositoryTest
{
    [TestFixture]
   public  class AddBookingTest
    {
        private NhSetup _obj;

        [SetUp]
        public void Init()
        {
            _obj = new NhSetup();
            _obj.Setup();
        }
        [Test]
        public void When_Assigning_Single_Room_ForBooking()
        {
            //Arrange
            var controller = new BookingApiController();
            var hotel = new Hotel() { Id = 1, Name = "Darshan", City = "Pune" };
            var roomList = new List<Room>()
            {
                new Room()
                {
                    Id = 1,
                    RoomNumber = 101,
                    RoomTariff = 200,
                    RoomType = new RoomType() {BedType = "2BED", Hotel = hotel, RoomCategory = "Std", Id = 1}
                },
                 new Room()
                {
                    Id = 2,
                    RoomNumber = 102,
                    RoomTariff = 200,
                    RoomType = new RoomType() {BedType = "2BED", Hotel = hotel, RoomCategory = "Std", Id = 1}
                },

            };
            var room = new Room()
            {
                Id = 2,
                RoomNumber = 102,
                RoomTariff = 200,
                RoomType = new RoomType() {BedType = "2BED", Hotel = hotel, RoomCategory = "Std", Id = 1}
            };
            var booking = new Booking() { CustomerName = "ABC", CheckInDate = DateTime.Now, CheckOutDate = DateTime.Now.AddDays(2), City = "pune", MobileNumber = "7588064015", Hotel = hotel, Status = HotelEnums.BookingStatus.Confirmed, UpdatetedOn = DateTime.UtcNow };
            var roomrelation = new RoomRelations() { Booking = booking, Room = room };
            var bookingViewModel = new BookingViewModel() { Booking = booking, Rooms = roomList };
         
            //Act
            var status = controller.SaveBooking(bookingViewModel);

            //Assert
            Assert.AreEqual(status.StatusCode, "Accepted");


        }

        [Test]
        public void When_Pass_Valid_MonthAndYear_ForGetMonthlyBooking()
        {
            //Arrange
            var controller = new BookingApiController();

            //Act
            controller.GetMonthlyBooking(Convert.ToDateTime("2014-06-10"));

        }
    }
}
