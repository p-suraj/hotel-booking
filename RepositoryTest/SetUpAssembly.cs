﻿using DataLayers.NhSetup;
using DataLayers.SharedUtility;
using NUnit.Framework;

namespace RepositoryTest
{
    [TestFixture]
    public class SetUpAssembly
    {

        private NhSetup _obj;

        [SetUp]
        public void InitSetUp()
        {
            var connectionString = ColloSysParam.WebParams.ConnectionString;

             _obj=new NhSetup();
            _obj.Setup();
        }

        [TestCase]
        public void GenrateDb()
        {

            _obj.GenrateDb();
        }

    }
}
