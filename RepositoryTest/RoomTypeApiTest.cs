﻿using DataLayer;
using DataLayer.SessionMgr;
using DataLayers.NhSetup;
using Hotel_Booking.WebApi;
using NUnit.Framework;

namespace RepositoryTest
{
    [TestFixture]
    public class RoomTypeApiTest
    {
        private NhSetup _obj;
        [SetUp]
        public void Init()
        {
            _obj = new NhSetup();
            _obj.Setup();
        }

        [Test]
        public void When_Assigning_Valid_EntityObject_To_RoomType()
        {
            var Controller = new RoomTypeApiController();

            var session = SessionManager.GetNewSession();

            var hotel = session.QueryOver<Hotel>().SingleOrDefault();

            RoomType obj = new RoomType { BedType = "2 bed", RoomCategory = "Standard", Hotel = hotel };

            session.Clear();
            Controller.SaveRoomType(obj);
        }

      
    
    }
}
