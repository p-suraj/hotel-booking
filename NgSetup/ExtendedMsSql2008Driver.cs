﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Driver;
using NHibernate.SqlTypes;

namespace NgSetup
{
    public class ExtendedMsSql2008Driver : Sql2008ClientDriver
    {
        protected override void InitializeParameter(IDbDataParameter dbParam, string name, SqlType sqlType)
        {
            if (Equals(sqlType, SqlTypeFactory.UInt16)) sqlType = SqlTypeFactory.Int32;
            if (Equals(sqlType, SqlTypeFactory.UInt32)) sqlType = SqlTypeFactory.Int64;
            if (Equals(sqlType, SqlTypeFactory.UInt64)) sqlType = SqlTypeFactory.Decimal;
            base.InitializeParameter(dbParam, name, sqlType);
        }
    }
}
