﻿
hotelapp.factory("$csShared", function () {
    var enums = {};
    return {
        enums: enums,
    };
});

hotelapp.factory("$csModels", [function () {

        var models = {};

        var init = function () {
           
          //  models.Generic = $csGenericModels.init();
            return;
        };

        var getTable = function (tableName) {
            switch (tableName) {
                //case "FileDetail":
                //    return angular.copy(models.FileUpload.FileDetail);
                
                default:
                    throw "Invalid Table Name : " + tableName + ".";
            }
        };

        var getColumns = function (tableName) {
            var table = getTable(tableName);
            return table.Columns;
        };

        var getBillingColumns = function () {
            var table = getTable("CustomerInfo");
            return table.Columns;
        };

        return {
            init: init,
            getTable: getTable,
            getColumns: getColumns,
            getBillingColumns: getBillingColumns
        };
    }
]);