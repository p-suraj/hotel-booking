﻿

hotelapp.factory("userDetailDatalayer", ["Restangular", "$csnotify", function (rest, $csnotify) {
    var dldata = {};
    dldata.hotelLists = {};
    dldata.hotelData = {};
    var api = rest.all("HotelApi");
    var apiuser = rest.all("UsersApi");

    var gethotel = function () {
        api.customGET("GetHotels").then(function (data) {
            dldata.hotelLists = data;
        });
    };

    var saveUser = function (userdetail) {
        apiuser.customPOST(userdetail, "SaveUsers").then(function () {
            $csnotify.success("User Details saved");
        }, function () {
            $csnotify.error("error during saved");
        });
    };

    var loginpost = function (login) {
        return apiuser.customPOST(login, "LoginUser").then(function (data) {
            dldata.hotelData = data;
            return data;
            // $csnotify.success("Login", "Login Successfully");
            //$scope.loginshow = false;
        }, function () {
            $csnotify.error("Error", "Wrong Username Or Password");

        });
    };
    return {
        dldata: dldata,
        gethotel: gethotel,
        saveUser: saveUser,
        loginpost: loginpost
    };
}]);

hotelapp.controller("userController", ["$scope", "$csnotify", "userDetailDatalayer", function ($scope, $csnotify, datalayer) {
    (function () {
        $scope.login = {};
        $scope.userdetl = {};
        $scope.datalayer = datalayer;
        $scope.dldata = datalayer.dldata;
      datalayer.gethotel();
        console.log($scope.dldata.hotelLists);
        $scope.ownerr = false;
        $scope.loginshow = true;
        $scope.menubar = false;
        $scope.userdarray = [];

        $scope.userdetails = {
            UserHotelname: { label: "Hotel Name", type: "select", textField: "Name", valuefield: "Name", required: true },
            UserType: { label: "Type", type: "text", required: true },
            UserName: { label: "Name", type: "text", required: true },
            UserUserid: { label: "UserName", type: "text", required: true },
            UserUserpassword: { label: "Password", type: "text", required: true },
            UserContact: { label: "Contact No.", type: "text", template: "phone", required: true, placeholder: "Allow only number" },
            UserEmail: { label: "email", type: "email", required: true, placeholder: "shree@gmail.com", patternMessage: 'Invalid Email' }
        };
    })();
    $scope.imageurl = 'http://localhost:8084/login-button.jpg';
    $scope.LoginFields = {
        Username: { type: "text", label: "Username"},
        Password: { type: "password",editable: false, label: "Password" },
    };
    // $scope.Hname = [];
    $scope.hotelDetails = [];
    $scope.loginsuccess = function (login) {
        debugger;
        datalayer.loginpost(login).then(function (data) {
            if (angular.isDefined(data)) {
                $scope.hotelDetails = data;
                if ($scope.hotelDetails.length !== "") {
                    $scope.Hname = data.Users;
                    //_.forEach(data.Users, function (userTypeObj) {
                    if (data.UserType === "Admin") {
                            $scope.admin = true;
                            $scope.user = false;
                        }
                        else {
                            $scope.admin = false;
                            $scope.user = true;
                        }
                    //});
                    $scope.roomtype = data.RoomTypes;
                    $scope.menubar = true;
                    $scope.loginshow = false;
                    $csnotify.success("Login", "Login Successfully");
                }
            }
        });
    };
    $scope.loginout = function () {
        $scope.menubar = false;
        $scope.loginshow = true;
        $scope.login.UserName = {};
        $scope.login.Password = {};
        $csnotify.success("Logout Success", "Thanks For Visit Hotels");
    };
    $scope.useradds = function (userdetl) {
        datalayer.saveUser(userdetl);
        $scope.userdetl = {};
    };
}]);