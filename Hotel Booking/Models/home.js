﻿var hotelapp = angular.module("hotelmodule", ['ui.bootstrap','ngRoute','restangular']);
hotelapp.filter('unique', function () {
    return function (input, key) {
        var unique = {};
        var uniqueList = [];
        for (var i = 0; i < input.length; i++) {
            if (typeof unique[input[i][key]] == "undefined") {
                unique[input[i][key]] = "";
                uniqueList.push(input[i]);
            }
        }
        return uniqueList;
    };
});

hotelapp.config(["$routeProvider", "RestangularProvider", function ($routeProvider, restangularProvider) {

    $routeProvider
        

      .when('/aaddhotel', {
          templateUrl: '/Home/addHotel.html',
          controller: 'hotel1Controller'
      })

      .when('/broom', {
          templateUrl: '/Home/Room.html',
          controller: 'RoomController'
      })
         .when('/broomtype', {
             templateUrl: '/Home/roomtype.html',
             controller: 'RoomTypeController'
         })
          .when('/cuser', {
              templateUrl: '/Home/UserDetail.html',
              controller: 'userController'
          })
          .when('/NewBooking', {
              templateUrl: '/Home/Booking.html',
              controller: 'bookingController'
          })
          .when('/TodayBooking', {
              templateUrl: '/Home/TodayBooking.html',
              controller: 'bookingController'
          })
         .when('/viewBookings', {
             templateUrl: '/Home/ViewAllBookings.html',
             controller: 'bookingController'
         })
        .when('/dailyReport', {
            templateUrl: '/Home/DailyReport.html',
            controller: 'reportController'
        })
        .when('/monthlyReport', {
            templateUrl: '/Home/MonthlyReport.html',
            controller: 'reportController'
        })
      .otherwise({ redirectTo: '/NewBooking' });

    restangularProvider.setBaseUrl( "api/");
}]);

hotelapp.controller("hotelcontroller", ["$scope", "$csfactory", "$csnotify",
    function ($scope, $csfactory, $csnotify) {
        //$scope.imageurl = 'http://localhost:8084/login-button.jpg';
        //$scope.recdetails = [];
        //$scope.userdetl = {};
        //$scope.roomtyp = {};
        //$scope.addhtl = {};
        //$scope.addhotels = {};
        //$scope.rbooking = {};
        //$scope.addroomtyp = {};

        

        //================  Login  ==============================
        //$scope.in = false;
        //$scope.im = true;
        //$scope.changeimage = function() {
        //    $scope.in = true;
        //    $scope.im = false;
        //};
        //$scope.login = {};
        //$scope.loginDetails = {
        //    UserUsername: { label: "Username", type: "text", required: true },
        //    UserPassword: { label: "Password", type: "text", required: true }
        //};

        //$scope.loginshow = true;
        //$scope.homegroup = false;
        ////$scope.message = false;
        //$scope.loginsuccess = function () {

        //    if ($scope.login.user === 'sss' && $scope.login.password === 'sss') {
               
        //        $scope.loginshow = false;
        //        $scope.homegroup = true;
        //        $csnotify.success('welcome admin', 'login successfully');
                
        //    }
        //    else {
            
        //          $csnotify.error("Error","wrong username or password");
               
        //    }
        //};
        //==============  Add hotel  ====================

//        $scope.hotelCities = ['Aurangabad', 'Bandra(Mumbai Suburban district)', 'Nagpur', 'Pune',
//' Akola', 'Chandrapur', ' Jalgaon', 'Parbhani', 'Solapur', 'Thane', 'Latur',
//'Mumbai-City', 'Buldhana', 'Dhule', ' Kolhpur', 'Nanded', ' Raigad',
//'Amravati', 'Nashik', 'Wardha', ' Ahmednagar', 'Beed', 'Bhandara', 'Gadchiroli', 'Jalna',
//'Osmanabad', 'Ratnagiri', 'Sangli', 'Satara', 'Sindudurg', 'Yavatmal', 'Nandurbar', 'ashim',
//'Gondia', 'Hingoli'];
//        $scope.hotel = {
//            hotelname: { label: "Hotel Name", type: "text", required: true },
//            hotelcity: { label: "City", type: "enum", valueList: $scope.hotelCities,required:true }
//        };
//        $scope.addhl = false;
//        $scope.count = 0;
//        $scope.hoteladds = function (addhtl) {
//            var checkroom = _.find($scope.recdetails, function (rmcheck) {
//                console.log("checking room id: ", rmcheck.rid);
//                if (rmcheck.hname === addhtl.hname) {
//                    $csnotify.error("Error", "hotel added already");
//                    return rmcheck;

//                }

//            });
//            if (angular.isUndefined(checkroom)) {
//                $scope.recdetails.push(angular.copy(addhtl));
//                $scope.count++;
//                $scope.addhl = true;
//                $scope.addhotels.hname = {};
//                $scope.addhotels.hcity = {};
//            }
          
//      };

        //============  user  master  ==============================================
      

        //$scope.userdetails = {
        //    UserHotelname: { label: "Hotel Name", type: "select", textField: "hname", valueField: "hname", valueList: $scope.recdetails,required:true },
        //    UserType: { label: "Type", type: "text", required: true },
        //    UserName: { label: "Name", type: "text", required: true },
        //    UserUserid: { label: "User Id", type: "text", required: true },
        //    UserUserpassword: { label: "Password", type: "text", required: true },
        //    UserContact: { label: "Contact No.", type: "text", template: "phone", required: true, placeholder: "Allow only number" },
        //    UserEmail: { label: "email", type: "email", required: true, placeholder: "shree@gmail.com", patternMessage: 'Invalid Email' }
        //};
        //$scope.ownerr = false;
        //$scope.userdarray = [];
        //$scope.useradds = function (userdetl) {
        //    console.log(userdetl);
        //    $scope.userdarray.push(angular.copy(userdetl));
        //    $scope.ownerr = true;
        //    $scope.userdetl.hotelnm = {};
        //    $scope.userdetl.utype = {};
        //    $scope.userdetl.uname = {};
        //    $scope.userdetl.uid = {};
        //    $scope.userdetl.upassword = {};
        //    $scope.userdetl.umob = {};
        //    $scope.userdetl.uemail = {};

        //};
       
        ////=======================room master=========================================
        ////==============room type detail==============================================
        //$scope.addshow = false;
        //$scope.addroomarray = [];
        //$scope.addroomdtls = function (addroomtyp) {
        //    debugger;
        //    $scope.addroomarray.push(angular.copy(addroomtyp));
        //    $scope.addshow = true;
        //    $scope.addroomtyp.addrhotelnm = {};
        //    $scope.addroomtyp.addrcategory = {};
        //    $scope.addroomtyp.addrbedtype = {};
        //};
       
        //$scope.gettype = function (obj) {
        //    $scope.getnewarray = [];
        //    _.forEach($scope.addroomarray, function(row) {
        //        if (row.addrhotelnm === obj.rhotelnm) {
                   
        //            var list = row.addrcategory + '(' + row.addrbedtype + ')';

        //            $scope.getnewarray.push(angular.copy(list));

        //        }
        //            });

         
        //};
        


        //$scope.roomcatg = ["Standard","Delux","Suite"];
        //$scope.roombed = ["2 bed","3 bed","5 bed","Hall"];
        //$scope.roomtypedetail = {
        //    roomHotelname: { label: "Hotel Name", type: "select", textField: "addrhotelnm", valueField: "addrhotelnm",valueList:$scope.addroomarray, required: "true" },
        //   roomCategory: { label: "Room type", type: "enum" , required: "true" },
        //    roomRoomno: { label: "Room No", type: "number", required: "true" }
            
        //};
        //$scope.typeshow = false;
        //$scope.roomtypearray = [];
        //$scope.roomdtls = function (roomtyp) {
        //    debugger;
        //    $scope.roomtypearray.push(angular.copy(roomtyp));
        //    $scope.typeshow = true;
        //    $scope.roomtyp.rhotelnm = {};
        //    $scope.roomtyp.rcategory = {};
        //    $scope.roomtyp.rroomno = {};
        //};
        ////==========================room add=======================================

      
        //$scope.addroomtypedetail = {
        //    addroomHotelname: { label: "Hotel Name", type: "select", textField: "hname", valueField: "hname", valueList: $scope.recdetails, required: "true" },
        //    addroomCategory: { label: "Category", type: "text", required: "true" },
        //    addroomBedtype: { label: "Bed Type", type: "text", required: "true" }
        //};
       


        //=========================booking table================================================
        //$scope.bookrooms = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        //$scope.bookStats = ["Pending","Confirmed","NotConfirmed"];
        //$scope.bookroomcatg = ["Standard", "Delux", "Suite"];
        //$scope.booking = {
        //    bookHotelname: { label: "Hotel Name", type: "select", textField: "hname", valueField: "hname", valueList: $scope.recdetails, required: "true" },
        //    bookroomtype: { label: "Room type", type: "enum", valueList: $scope.bookroomcatg, required: "true" },
            
        //    bookCustname: { label: "Customer Name", type: "text", required: true },
        //    bookContact: { label: "Contact No.", type: "text", template: "phone", required: true, placeholder: "Allow only number" },
        //    bookcustcity: { label: "Customer city", type: "text", required: true },
        //    bookcheckindate: { label: "CheckIn Date", type: "date" },
        //    bookcheckoutdate: { label: "CheckOut Date", type: "date" },
        //    bookstatus: { label: "Status", type: "enum", valueList: $scope.bookStats, required: "true" },
        //    bookroom: { label: "Room Id", type: "enum", valueList: $scope.bookrooms, required: "true" }
        //};
        //$scope.bookshow = false;
        //$scope.bookingarray = [];
        //$scope.bookingreq = function (rbooking) {
        //    debugger;
        //    $scope.bookingarray.push(angular.copy(rbooking));
        //    $scope.bookshow = true;

        //};
        //=================== Homepage  button  ===================================

        //$scope.addhotl = false;
        //$scope.userdt = false;
        //$scope.roomdtl = false;
        //$scope.rmbook = false;
        //$scope.addhotel = function () {
        //    $scope.addhotl = true;
        //    $scope.userdt = false;
        //    $scope.roomdtl = false;
        //    $scope.rmbook = false;
        //};
        //$scope.userdetail = function () {
           
        //    $scope.userdt = true;
        //    $scope.roomdtl = false;
        //    $scope.addhotl = false;
        //    $scope.rmbook = false;
        //};
        //$scope.roomdetail = function () {
        //    $scope.roomdtl = true;
        //    $scope.userdt = false;
        //    $scope.addhotl = false;
        //    $scope.rmbook = false;
        //};
        //$scope.bookings = function () {
        //    $scope.rmbook = true;
        //    $scope.roomdtl = false;
        //    $scope.userdt = false;
        //    $scope.addhotl = false;
        //};
        //$scope.rmad = true;
        //$scope.rtyp = true;
        //$scope.roomadd = function() {

        //    $scope.rmad = false;
        //    $scope.rtyp = true;
        //};
        //$scope.roomtp = function() {
        //    $scope.rtyp = false;
        //    $scope.rmad = true;
        //};

    }

]);


