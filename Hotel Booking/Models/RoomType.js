﻿//DataLayer
hotelapp.factory("RoomTypeDataLayer", [
    "Restangular", "$csnotify", function (rest, $csnotify) {

        var api = rest.all("RoomTypeApi");
        var hotelApi = rest.all("HotelApi");
        var dldata = {};

        dldata.RoomTypeLists = [];
        dldata.RoomTypeSave = [];
        dldata.hotelLists = [];

        var getHotel = function() {
          return  hotelApi.customGET("GetHotels").then(function(data) {
                dldata.hotelLists = data;
                return data;
            });
        };

        var saveRoomType = function (roomTypeObj) {
            debugger;
          return   api.customPOST(roomTypeObj, "SaveRoomType").then(function (data) {
                $csnotify.success("saved", "RoomType saved successfully");
              return data;
                //dldata.RoomTypeSave = data;
            });

        };
        var getRoomTypeList = function () {
           return  api.customGET("GetRoomTypes").then(function (data) {
                dldata.RoomTypeLists = data;
               return data;
                //console.log(dldata.hotelLists);
            });
        };
        return {
            dldata: dldata,
            gethotel: getHotel,
            saveroomtype: saveRoomType,
            getRoomTypeList: getRoomTypeList
        };

    }
]);

hotelapp.controller("RoomTypeController", ["$scope", "$csnotify", "RoomTypeDataLayer", "userDetailDatalayer", function ($scope, $csnotify, datalayer, roomDatalayer) {

    (function () {
        $scope.datalayer = datalayer;
        $scope.dldata = datalayer.dldata;
        $scope.hotelArray = [];
        debugger;
        datalayer.gethotel().then(function(data) {
            $scope.allHotelData = data;
            _.forEach($scope.allHotelData, function(hotelName) {
                var hotelObj = {
                    Name: hotelName.Name,
                    Hotel: hotelName
                };
                $scope.hotelArray.push(hotelObj);
            });
        });
        $scope.roomDatalayer = roomDatalayer;
        $scope.roomDatalayer = roomDatalayer.dldata;
        $scope.addroomtyp = {};

        $scope.NameHotel = roomDatalayer.dldata.hotelData;
       // $scope.addroomtyp.Hotel = roomDatalayer.dldata.hotelData;
     //   $scope.Hotels = datalayer.gethotel;
      

    })();


    //$scope.addroomtyp.Hotel = {};
    //$scope.sam = function (obj) {
    //    debugger;
    //    var r = obj;
    //    $scope.addroomtyp.Hotel = r;
    //};
    $scope.addroomtypedetail = {
        addroomHotelname: { label: "Hotel Name", type: "select", textField: "Name", valuefield: "Name"},
        addroomCategory: { label: "Category", type: "text", required: "true" },
        addroomBedtype: { label: "Bed Type", type: "text", required: "true" }
    };

    $scope.addroomdtls = function (addroomtyp) {
        debugger;
        datalayer.saveroomtype(addroomtyp).then(function () {
            $scope.addroomtyp = {};
            
        });
      //  $scope.addroomtyp = {};
    };

}])