﻿
hotelapp.factory("bookingDatalayer", ["Restangular", "$csnotify", function (rest, $csnotify) {
    var apiBooking = rest.all("BookingApi");
    var api = rest.all("HotelApi");
    var apiRoomType = rest.all("RoomTypeApi");
    var apiRoom = rest.all("RoomApi");
    var dldata = {};

    dldata.hotelLists = [];
    dldata.hotelsaveddata = [];
    dldata.BookingSave = [];
    dldata.roomTypeLists = [];
    dldata.getStatusEnum = [];
    dldata.getRoomNumbers = [];
    dldata.hotelData = [];
    dldata.hotelRoomInfo = [];
    var getBooking = function (id) {
        return apiBooking.customGET("GetBookings", { id: id }).then(function (data) {
            return data;
            // dldata.hotelLists = data;

        });
    };
    var getAllBooking = function (id) {
        return apiBooking.customGET("GetAllBooking", { id: id }).then(function (data) {
            return data;
        });
    };
    var getRoomInfo = function (date, Id) {
        return apiBooking.customGET("GetTodayBooking", { date: date, Id: Id }).then(function (data) {
            //  dldata.hotelRoomInfo = data;
            // console.log(dldata.hotelRoomInfo);
            return data;
        });
    };

    var getHotelById = function () {
        api.customGET("GetHotelById").then(function (data) {
            dldata.hotelData = data;

        });
    };

    var saveBooking = function (obj) {
        return apiBooking.customPOST(obj, "SaveBooking").then(function (data) {
            $csnotify.success("saved", "Booking Created successfully");
            return data;
        });
    };

    var getRoomType = function (id) {
        return apiRoomType.customGET("GetRoomsAndTypeByHotelId", { id: id }).then(function (data) {
            dldata.roomTypeLists = data;
            return data;
        });
    };

    var getStatusEnum = function () {
        apiBooking.customGET("GetStatusEnum").then(function (data) {
            dldata.getStatusEnum = data;

        });
    };
    var getRoomNumbers = function () {
        apiRoom.customGET("GetRooms").then(function (data) {
            dldata.getRoomNumbers = data;
        }
        );
    };

    var updateStatus = function (obj) {
        return apiBooking.customPOST(obj, "UpdateStatus").then(function (data) {

            return data;
        });
    };
    var bookingUpdate = function (obj) {
        return apiBooking.customPOST(obj, "BookingUpdate").then(function (data) {
            $csnotify.success("saved", "Booking Update successfully");
            return data;
        });
    };

    return {
        dldata: dldata,
        saveBooking: saveBooking,
        getBooking: getBooking,
        getRoomType: getRoomType,
        getStatusEnum: getStatusEnum,
        getRoomNumbers: getRoomNumbers,
        getHotelById: getHotelById,
        getRoomInfo: getRoomInfo,
        updateStatus: updateStatus,
        bookingUpdate: bookingUpdate,
        getAllBooking: getAllBooking
    };
}]);

hotelapp.controller("bookingController", ["$scope", "$csnotify", "bookingDatalayer", "userDetailDatalayer", function ($scope, $csnotify, datalayer, userdatalayer) {
    $scope.months = [
    { Month: 'January', Value: 1 },
    { Month: 'February', Value: 2 }, { Month: 'March', Value: 3 }, { Month: 'April', Value: 4 },
    { Month: 'May', Value: 5 }, { Month: 'June', Value: 6 }, { Month: 'July', Value: 7 },
     { Month: 'August', Value: 8 }, { Month: 'September', Value: 9 }, { Month: 'Octomber', Value: 10 },
      { Month: 'November', Value: 11 }, { Month: 'December', Value: 12 }
    ];
    $scope.years = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025];

    $scope.dateBooking = {

        bookMonth: {
            label: "Month", type: "select", textField: "Month", valueField: "Value", required: true, valueList: $scope.months
        },
        bookDay: { label: "Day", type: "select", valueList: [] },
        bookYear: { label: "Year", type: "enum", required: true, valueList: $scope.years }
    };
    $scope.checkDate = function (date) {
        if (date === 0) return "";
        else return date;
    };
    $scope.checkBookcount = function (bCount) {
        if (bCount > 0) return "B-" + bCount;
        else return "";
    };

    var getDateRoom = function (date) {
        var countBook = [];
        var bookingArray = $scope.checkinDateCall;

        _.forEach(bookingArray, function (checkdate) {
            if (checkdate.CheckInDate === date) {
                countBook.push(checkdate);
            }
        });

        if (countBook.length === 0) {

            return "";
        }
        else {

            return countBook.length;
        }
    };
    $scope.tableshow = false;
    $scope.eventfun = function (dbook) {
        debugger;
        var day = moment(dbook.dmonth + /'01'/ + dbook.dyear).format("dddd");
        var week = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var weekselect = week.indexOf(day);
        $scope.result = [];
        if (dbook.dmonth === 2) {
            if (dbook.dyear % 4 === 0) {
                var leap = 29;
                $scope.result.push(leap);
            }
            else {
                $scope.result = 28;
            }
        }
        else {
            $scope.result = new Date(dbook.dyear, dbook.dmonth, 0).getDate();
        };

        $scope.dateSelection = [];

        for (var d = 1; d <= weekselect; d++) {
            $scope.dateSelection.push(0);
        };
        for (var b = 1; b <= $scope.result; b++) {
            var k = b;
            var dayMonth = dbook.dmonth + "/" + b + "/" + dbook.dyear;
            var dayFun = moment(dayMonth).format('YYYY-MM-DDT00:00:00');

            var roomcnt = getDateRoom(dayFun);
            var dateObj = {
                dates: b,
                countDay: roomcnt
            };
            $scope.dateSelection.push(dateObj);

        };
        $scope.firstarrays = [];
        for (var i = 1; i <= 7; i++) {
            var c = $scope.dateSelection[i - 1];

            $scope.firstarrays.push(c);
        };

        $scope.secondarray = [];
        for (var s = 8; s <= 14; s++) {
            var t = $scope.dateSelection[s - 1];

            $scope.secondarray.push(t);
        };
        $scope.thirdarray = [];
        for (var w = 15; w <= 21; w++) {
            var q = $scope.dateSelection[w - 1];

            $scope.thirdarray.push(q);
        };
        $scope.fourarray = [];
        for (var z = 22; z <= 28; z++) {
            var v = $scope.dateSelection[z - 1];

            $scope.fourarray.push(v);
        };
        $scope.fivearray = [];
        for (var g = 29; g <= 35; g++) {
            var m = $scope.dateSelection[g - 1];
            $scope.fivearray.push(m);
        }
        $scope.sixarray = [];
        for (var ag = 36; ag <= 42; ag++) {
            var ss = $scope.dateSelection[ag - 1];
            $scope.sixarray.push(ss);
        }
    };
    $scope.calenderForm = true;
    $scope.newBookingForm = false;
    $scope.bookTable = false;
    $scope.newBook = function () {
        $scope.newBookingForm = true;
        $scope.calenderForm = false;
        //  $scope.dateOption = false;
        $scope.newBookButton = false;
        $scope.viewBookButton = false;
    };
    $scope.viewBook = function () {
        $scope.bookTable = true;
        $scope.newBookingForm = false;
        $scope.calenderForm = false;
        // $scope.dateOption = false;
        $scope.newBookButton = false;
        $scope.viewBookButton = false;
    };
    $scope.backCalender = function () {
        $scope.calenderForm = true;
        //  $scope.dateOption = false;
        $scope.newBookButton = false;
        $scope.viewBookButton = false;
        $scope.newBookingForm = false;
        $scope.bookTable = false;
               _.forEach($scope.roomdata, function (roomnum) {
            _.forEach($scope.RoomCharts, function (room) {
                if (room.Rm === roomnum.RoomNumber) {
                    room.click = '';
                }
           
            });
        });
        $scope.roomdata = [];
        $scope.countBook = {};
        $scope.rbooking = {};

    };
    $scope.newBookingForm = false;
    $scope.newBookButton = false;
    $scope.viewBookButton = false;
    // $scope.dateOption = false;
    $scope.bookTable = false;
    $scope.joindate = function (date) {
        var dateIncrement = date + 1;
        $scope.dateDisplay = [];
        $scope.datejoin = $scope.dbook.dmonth + "/" + date + "/" + $scope.dbook.dyear;
        $scope.chekingDate($scope.datejoin);
        $scope.dateDisplay = moment($scope.datejoin).format('MMMM DD, YYYY');
        $scope.receiveDate = moment($scope.datejoin).format('YYYY-MM-DD');
        $scope.currentdate = moment().format('YYYY-MM-DD');
        $scope.dateDisplayIncrement = [];
        $scope.datejoins = $scope.dbook.dmonth + "/" + dateIncrement + "/" + $scope.dbook.dyear;
        $scope.dateDisplayIncrement = moment($scope.datejoins).format('MMMM DD, YYYY');
        if ($scope.currentdate <= $scope.receiveDate) {
            //$scope.dateOption = true;
            $scope.newBookButton = true;
            $scope.viewBookButton = true;
            $scope.newBookingForm = false;

        } else {
            $csnotify.success("Warning", "Date must be greater than today for Booking");
            //  $scope.dateOption = false;
            $scope.newBookButton = false;
            $scope.viewBookButton = true;
            $scope.newBookingForm = false;
        }
        $scope.rbooking.CheckInDate = $scope.dateDisplay;
        $scope.rbooking.CheckOutDate = $scope.dateDisplayIncrement;
        $scope.rbooking.UpdatetedOn = $scope.dateDisplay;
    };
    $scope.bookingreq = function (obj) {
        debugger;
        var saveData = {};
        var bookingObj = { CheckInDate: obj.CheckInDate, CheckOutDate: obj.CheckOutDate, City: obj.City, Comment: obj.Comment, CustomerName: obj.CustomerName, MobileNumber: obj.MobileNumber, UpdatetedOn: obj.UpdatetedOn, Hotel: userdatalayer.dldata.hotelData.Hotel };
        saveData.Booking = bookingObj;
        saveData.Rooms = $scope.roomdata;
        datalayer.saveBooking(saveData).then(function (data) {
            $scope.getBookingData = data;
            $scope.calenderForm = true;
            // $scope.dateOption = false;
            $scope.newBookButton = false;
            $scope.viewBookButton = false;
            $scope.newBookingForm = false;
            $scope.bookTable = false;
            $scope.checkedModel = {};
            $scope.rbooking = {};
            $scope.countBook = {};
            $scope.roomdata = [];
           var id = userdatalayer.dldata.hotelData.Hotel.Id;
            datalayer.getBooking(id).then(function (datas) {
                $scope.getBookDetails = datas;
                $scope.checkinDateCall = datas.Bookings;
                var currentMonth = moment().month() + 1;
                $scope.dbook.dmonth = currentMonth;
                var currentYear = moment().year();
                $scope.dbook.dyear = currentYear;
                $scope.eventfun($scope.dbook);
                var todayDate = moment().format('YYYY-MM-DD');
                $scope.getDates(todayDate);
            });
        });
    };

    $scope.gethotelId = function (obj) {
        $scope.arrayname = [];
        _.forEach(userdatalayer.dldata.hotelData.RoomTypes, function (categoryBed) {
            var x = categoryBed.RoomCategory + '(' + categoryBed.BedType + ')';
            var objc = {
                Value: categoryBed,
                Name: x
            };
            $scope.arrayname.push(objc);
        });
    };
    $scope.getRoomNo = function (data) {
        $scope.arrayRooms = [];
        var c = userdatalayer.dldata.hotelData.RoomTypes;
        _.forEach(c, function (objRoom) {
            if ($scope.rbooking.Hotel.Name === userdatalayer.dldata.hotelData.Name && objRoom.RoomCategory === data.RoomCategory && objRoom.BedType === data.BedType) {
                _.forEach(objRoom.Rooms, function (roomobj) {
                    var b = roomobj.RoomNumber;
                    var getRoomObj = {
                        row: roomobj,
                        DispName: b
                    };
                    $scope.arrayRooms.push(getRoomObj);
                });
            }
        });
    };
    (function () {
        $scope.datalayer = datalayer;
        $scope.dldata = datalayer.dldata;
        $scope.userdatalayer = userdatalayer;
        $scope.userdatalayer = userdatalayer.dldata;
        $scope.getTypeRoom = [];
        $scope.checkinDateCall = [];
        $scope.bookDetail = [];
        $scope.upDate = [];
        var Id = userdatalayer.dldata.hotelData.Hotel.Id;
        datalayer.getRoomType(Id).then(function (data) {
            $scope.getTypeRoom = data;
            $scope.findRoomChart($scope.getTypeRoom);
        });
        $scope.gethotelId(userdatalayer.dldata.hotelData);
        $scope.rbooking = {};
        $scope.dbook = {};
        $scope.rbooking.Hotel = userdatalayer.dldata.hotelData.Hotel;
        $scope.bookshow = false;
        $scope.bookingarray = [];
        $scope.roomdata = [];
        $scope.changeRoomdata = [];
        $scope.roomtypearray = [];
        datalayer.getStatusEnum();
        datalayer.getRoomNumbers();
        var currentMonth = moment().month() + 1;
        $scope.dbook.dmonth = currentMonth;
        var currentYear = moment().year();
        $scope.dbook.dyear = currentYear;
        var currentDay = moment().format('D');
        $scope.day = currentDay;
        $scope.month = currentMonth;
        $scope.year = currentYear;
        $scope.Todaydate = moment().format('MMMM Do YYYY, h:mm a');
        $scope.eventfun($scope.dbook);
        $scope.datePickerSelect = {};
        datalayer.getBooking(Id).then(function (data) {
            $scope.getBookDetails = data;
            $scope.checkinDateCall = data.Bookings;
            $scope.eventfun($scope.dbook);
            var todayDate = moment().format('YYYY-MM-DD');
            $scope.getDates(todayDate);
        });
        var id = userdatalayer.dldata.hotelData.Hotel.Id;
        datalayer.getAllBooking(id).then(function (data) {
            $scope.allBookingData = _.sortBy(data.RoomRelations, 'Booking.CheckInDate');
            $scope.viewAllBooking($scope.allBookingData);
        });
        $scope.booking = {
            bookHotelname: { label: "Hotel Name", type: "text" },
            bookRoomCount: { label: "Total Room", type: "number" },
            //bookroomCategory: { label: "Room Type", type: "select", textField: "Name", valueField: "Value", required: true },
            bookCustname: { label: "Customer Name", type: "text", required: true },
            bookContact: { label: "Mobile No", type: 'text', template: 'user' },
            bookcustcity: { label: "Customer city", type: "text", required: true },
            bookroom: { label: "Room Number", type: "select", textField: "DispName", valueField: "row" },
            bookcheckDate: { label: "CheckIn Date", type: "text", required: "true" },
            bookcheckOutDate: { label: "CheckOut Date", type: "text", required: "true" },
            bookcheckUpdateDate: { label: "update Date", type: "text" },
            bookComment: { label: "Comment", type: "textarea", required: true }
        };

    })();
    $scope.update = {
        bookHotelname: { label: "Hotel Name", type: "text", required: true },
        bookRoomCount: { label: "Total Room", type: "number" },
        //bookroomCategory: { label: "Room Type", type: "select", textField: "Name", valueField: "Value", required: true },
        bookCustname: { label: "Customer Name", type: "text" },
        bookContact: { label: "Contact No.", type: "text" },
        bookcustcity: { label: "Customer city", type: "text" },
        bookroom: { label: "Room Number", type: "number" },
        bookcheckDate: { label: "CheckIn Date", type: "text", required: "true" },
        bookcheckOutDate: { label: "CheckOut Date", type: "text", required: "true" },
        bookcheckUpdateDate: { label: "update Date", type: "text" }
    };

    $scope.viewAllBooking = function (information) {
        $scope.viewAllBookingArray = [];
        _.forEach(information, function (informationObj) {
            var informationObjs = {
                CustomerName: informationObj.Booking.CustomerName,
                City: informationObj.Booking.City,
                MobileNumber: informationObj.Booking.MobileNumber,
                Status: informationObj.Status,
                RoomNumber: informationObj.Room.RoomNumber,
                CheckInDate: moment(informationObj.Booking.CheckInDate).format('L')
            };
            $scope.viewAllBookingArray.push(informationObjs);
        });
    };

    //====================================  Room chart =======================================================

    $scope.RoomCharts = [];
    $scope.findRoomChart = function (roomTypes) {
        _.forEach(roomTypes, function (roomtype) {
            var r = roomtype.Rooms;
            _.forEach(roomtype.Rooms, function (objc) {
                var dataRoom = { catg: roomtype.RoomCategory, bedtyp: roomtype.BedType, Rm: objc.RoomNumber, RoomObj: r };
                $scope.RoomCharts.push(dataRoom);
            });
        });
    };
    $scope.roomArray = [];
    $scope.formshow = false;
    $scope.chart = true;
    $scope.updateButton = true;
    $scope.checkInButton = true;
    $scope.checkOutButton = true;
    $scope.cancelButton = true;
    $scope.changeRooms = true;
    $scope.selectRoomNumber = [];
    $scope.tempArray = [];
    //$scope.roomdata = [];
    $scope.chartDetail = function (chartObj, checked) {
        debugger;
        if (chartObj.status === "Book") {
            $csnotify.error("Error", "Room is already Book");
        }
        else {
            var roomData = _.find(chartObj.RoomObj, function (room) {
                if (room.RoomNumber === chartObj.Rm) {
                    return room;
                }
            });
            _.forEach($scope.roomdata, function (roomnum) {
                _.forEach($scope.RoomCharts, function (room) {
                    if (roomnum.RoomNumber === room.Rm) {
                        chartObj.click = '';
                    }
               
            });
             });
          
            var differentRooms = _.find($scope.roomdata, function (object) {
                if (object.RoomNumber === chartObj.Rm) {
                    var output = _.pluck($scope.roomdata, 'RoomNumber');
                   var index = output.indexOf(roomData.RoomNumber);
                    if (index !== -1) {
                        $scope.roomdata.splice(index, 1);
                    }
                    return object;
                   
                }
            });
            if (angular.isUndefined(differentRooms)) {
                $scope.roomdata.push(roomData);
                _.forEach($scope.roomdata, function (roomnum) {
               _.forEach($scope.RoomCharts, function (room) {
                   if (room.Rm === roomnum.RoomNumber) {
                        chartObj.click = 'true';
                    }
               });
                });
            }
            $scope.countBook = $scope.roomdata.length;
        }
   };
    
    //$scope.chartDetail = function (chartObj, checked) {
    //    debugger;
    //    var roomData = _.find(chartObj.RoomObj, function (room) {
    //        if (room.RoomNumber === chartObj.Rm) {
    //            return room;
    //        }
    //    });
    //    if (!checked) {
    //        //push
    //        $scope.roomdata.push(roomData);
    //    }
    //    if (checked) {
    //        //find index of current room in array and splice
    //        var index = $scope.roomdata.indexOf(roomData.RoomNumber);
    //        $scope.roomdata.splice(index);
    //    }
    //    $scope.countBook = $scope.roomdata.length;
    //};

    $scope.roomchartDetail = function (chartObj, checked) {
        debugger;
        _.forEach($scope.bookDetail, function (obj) {
            if (chartObj.Rm === obj.Room.RoomNumber) {
                if (obj.Status === "CheckIn") {
                    $scope.checkOutButton = true;
                    $scope.updateButton = false;
                    $scope.checkInButton = false;
                    $scope.cancelButton = false;
                    $scope.changeRooms = false;
                }
                if (obj.Status === "Booked") {
                    $scope.updateButton = true;
                    $scope.checkInButton = true;
                    $scope.checkOutButton = false;
                    $scope.cancelButton = true;
                    $scope.changeRooms = true;
                }
            }
        });
        if (chartObj.status !== "Book") {
            $scope.upDate.CustomerName = "";
            $scope.upDate.MobileNumber = "";
            $scope.upDate.City = "";
            $scope.upDate.CheckInDate = "";
            $scope.upDate.RoomNumber = "";

        }
        else {
            $scope.formshow = true;
            $scope.chart = false;
            $scope.upDate.CustomerName = chartObj.book.Booking.CustomerName;
            $scope.upDate.MobileNumber = chartObj.book.Booking.MobileNumber;
            $scope.upDate.City = chartObj.book.Booking.City;
            $scope.upDate.Comment = chartObj.book.Booking.Comment;
            $scope.upDate.CheckInDate = moment(chartObj.book.Booking.CheckInDate).format('MMMM DD, YYYY');
            $scope.checkInDate = $scope.upDate.CheckInDate;
            $scope.upDate.CheckOutDate = moment(chartObj.book.Booking.CheckOutDate).format('MMMM DD, YYYY');
            $scope.upDate.UpdatetedOn = moment(chartObj.book.Booking.UpdatetedOn).format('MMMM DD, YYYY');
            $scope.RoomNumber = chartObj.book.Room.RoomNumber;
            $scope.RoomRelationId = chartObj.book.Id;
            $scope.BookingStatus = chartObj.book.Booking.Status;
            $scope.HotelObj = chartObj.book.Booking.Hotel;
            $scope.RoomRelationStatus = chartObj.book.Status;
            if (chartObj.book.Room.RoomNumber === chartObj.Rm) {
                $scope.getRoomId = chartObj.book.Room.Id;
                $scope.getBookId = chartObj.book.Booking.Id;
                $scope.getStatus = "CheckIn";
            }
        }
        var roomData = _.find(chartObj.RoomObj, function (room) {
            if (room.RoomNumber === chartObj.Rm) {
                return room;
            }
        });
        if (!checked) {
            //push
            $scope.roomdata.push(roomData);
        }
        if (checked) {
            //find index of current room in array and splice
            var index = $scope.roomdata.indexOf(roomData.RoomNumber);
            $scope.roomdata.splice(index);
        }
        $scope.countBook = $scope.roomdata.length;
    };


    $scope.isCollapsed = false;
    $scope.clickCheckInDate = function () {
        debugger;
        var saveObj = {};
        var obj = {
            RoomId: $scope.getRoomId,
            BookId: $scope.getBookId,
            Status: "CheckIn"
        };
        saveObj.status = obj;
        datalayer.updateStatus(saveObj.status).then(function () {
            $csnotify.success("Success", "CheckIn Booking Successfully");
            $scope.getDates($scope.checkInDate);
            $scope.formshow = false;
            $scope.chart = true;
        });
    };

    $scope.clickCheckOutDate = function () {

        var saveObjs = {};
        var obj = {
            RoomId: $scope.getRoomId,
            BookId: $scope.getBookId,
            Status: "CheckOut"
        };
        saveObjs.status = obj;
        datalayer.updateStatus(saveObjs.status).then(function () {

            $csnotify.success("Success", "CheckOut Booking Successfully");
            $scope.getDates($scope.checkInDate);
            $scope.formshow = false;
            $scope.chart = true;
        });
    };

    $scope.clickCancelDate = function () {
        var cancel = {};
        var obj = {
            RoomId: $scope.getRoomId,
            BookId: $scope.getBookId,
            Status: "Canceled"
        };
        cancel.status = obj;
        datalayer.updateStatus(cancel.status).then(function () {
            $csnotify.error("Canceled", "Canceled Booking Successfully");
            $scope.getDates($scope.checkInDate);
            $scope.formshow = false;
            $scope.chart = true;
        });
    };
    $scope.backChart = function () {
        $scope.formshow = false;
        $scope.chart = true;
    };
    $scope.datepicker = {
        dateP: { label: 'Select Date', type: 'date', defaultDate: 'today' }
    };
    $scope.chekingDate = function (obj) {
        setBookingStatus($scope.RoomCharts);

        var date = moment(obj).format('YYYY-MM-DD');
        var id = userdatalayer.dldata.hotelData.Hotel.Id;
        datalayer.getRoomInfo(date, id).then(function (objs) {
            $scope.bookDetails = objs;
            $scope.bookDetail = objs.RoomRelations;
            $scope.bookCall($scope.bookDetail);
            $scope.viewBooking($scope.bookDetail);
        });
    };
    $scope.getDates = function (obj) {
        setBookingStatus($scope.RoomCharts);
        var date = moment(obj).format('YYYY-MM-DD');
        $scope.currentdate = date;
        var id = userdatalayer.dldata.hotelData.Hotel.Id;
        datalayer.getRoomInfo(date, id).then(function (objs) {
            $scope.bookDetails = objs;
            $scope.bookDetail = objs.RoomRelations;
            $scope.bookCall($scope.bookDetail);
        });
    };
    $scope.bookCall = function (objGet) {
        _.forEach(objGet, function (getobj) {
            //  _.forEach(getobj, function (r) {
            _.forEach($scope.RoomCharts, function (room) {
                if (room.Rm === getobj.Room.RoomNumber) {
                    if (getobj.Status !== "CheckOut") {
                        room.status = "Book";
                        room.book = getobj;
                    } else {
                        room.status = "";
                    }
                }
            });

            //  });

        });
        $scope.roomsObject = [];
        _.forEach($scope.RoomCharts, function (roomObj) {

            if (roomObj.status === "") {
                $scope.roomsObject.push(roomObj);
            }
        });
    };
    var setBookingStatus = function (array) {
        _.forEach(array, function (item) {
            item.status = "";
        });
    };

    $scope.viewBooking = function (information) {
        $scope.viewBookingArray = [];
        _.forEach(information, function (informationObj) {
            var informationObjs = {
                CustomerName: informationObj.Booking.CustomerName,
                City: informationObj.Booking.City,
                MobileNumber: informationObj.Booking.MobileNumber,
                Status: informationObj.Status,
                Comment:informationObj.Booking.Comment,
                RoomNumber: informationObj.Room.RoomNumber
            };
            $scope.viewBookingArray.push(informationObjs);
        });
    };
    $scope.updateButton = true;
    $scope.getChangeRoomNo = [];
    $scope.changeRoom = function (obj, checking) {
        debugger;
        if (obj.status === "Book") {
            $csnotify.error("Error", "Room Booked...Choose Another");
        }
        else {
            var changeroomData = _.find(obj.RoomObj, function (room) {
                if (room.RoomNumber === obj.Rm) {
                    return room;
                }
            });
            $scope.getChangeRoomNo = changeroomData;
            $scope.RoomNumber = changeroomData.RoomNumber;
        }


        //if (!checking) {
        //    $scope.changeRoomdata.push(changeroomData);
        //   }
        // if (checking) {
        //     //find index of current room in array and splice
        //     var index = $scope.changeRoomdata.indexOf(changeroomData.RoomNumber);
        //     $scope.changeRoomdata.splice(index);
        //   }

    };

    $scope.updateInfo = function (obj) {
        var updateData = {};
        updateData.Booking = {};
        updateData.Booking = obj;
        updateData.Id = $scope.RoomRelationId;
        updateData.Status = $scope.RoomRelationStatus;
        updateData.Booking.Id = $scope.getBookId;
        updateData.Booking.Comment = $scope.upDate.Comment;
        updateData.Booking.Status = $scope.BookingStatus;
        updateData.Booking.Hotel = userdatalayer.dldata.hotelData.Hotel;
        $scope.currentCheckInDate = updateData.Booking.CheckInDate;
        if ($scope.getChangeRoomNo.length === 0) {
            updateData.Room = $scope.roomdata;
            _.forEach($scope.roomdata, function (roomObj) {
                $scope.RoomId = roomObj.Id;
                $scope.getRoomId = roomObj.Id;
                $scope.RoomNumber = roomObj.RoomNumber;
                $scope.RoomTariff = roomObj.RoomTariff;
            });
        }
        else {
            updateData.Room = $scope.getChangeRoomNo;
            $scope.RoomId = $scope.getChangeRoomNo.Id;
            $scope.getRoomId = $scope.getChangeRoomNo.Id;
            $scope.RoomNumber = $scope.getChangeRoomNo.RoomNumber;
            $scope.RoomTariff = $scope.getChangeRoomNo.RoomTariff;
        }
        var updateDataObj = updateData;
        $scope.getBookId = updateDataObj.Booking.Id;
        var getUpdateObject = {
            Booking: {
                CustomerName: updateDataObj.Booking.CustomerName,
                City: updateDataObj.Booking.City,
                CheckInDate: updateDataObj.Booking.CheckInDate,
                CheckOutDate: updateDataObj.Booking.CheckOutDate,
                UpdatetedOn: updateDataObj.Booking.UpdatetedOn,
                Id: updateDataObj.Booking.Id,
                Comment:updateData.Booking.Comment,
                Status: updateDataObj.Booking.Status,
                MobileNumber: updateDataObj.Booking.MobileNumber,
                Hotel: updateDataObj.Booking.Hotel
            },
            Id: updateDataObj.Id,
            Room: {
                Id: $scope.RoomId,
                RoomNumber: $scope.RoomNumber,
                RoomTariff: $scope.RoomTariff,
            },
            Status: updateDataObj.Status,
        };
        datalayer.bookingUpdate(getUpdateObject).then(function (data) {
            $scope.UpdatedData = data;
            $scope.getDates(updateDataObj.Booking.CheckInDate);
        });
    };
}]);

