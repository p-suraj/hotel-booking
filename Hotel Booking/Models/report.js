﻿hotelapp.factory("reportDatalayer", ["Restangular", "$csnotify", function (rest, $csnotify) {
    var apiBooking = rest.all("BookingApi");
    var apiRoomType = rest.all("RoomTypeApi");
    var dldata = {};
    dldata.hotelLists = [];
    dldata.hotelsaveddata = [];
    var dailyreport = function (date, Id) {
        return apiBooking.customGET("Reports", { date: date, Id: Id }).then(function (data) {
            return data;
        });
    };

    var monthlyreport = function (date, Id) {
        return apiBooking.customGET("MonthlyReports", { date: date, Id: Id }).then(function (data) {
            return data;
        });
    };

    var getRoomTypes = function (id) {
        return apiRoomType.customGET("GetRoomsAndTypeByHotelId", { id: id }).then(function (data) {

            return data;
        });
    };

    return {
        dailyreport: dailyreport,
        getRoomTypes: getRoomTypes,
        monthlyreport: monthlyreport
    };
}]);
hotelapp.controller("reportController", ["$scope", "$csnotify", "reportDatalayer", "userDetailDatalayer", function ($scope, $csnotify, datalayer, userdatalayer) {
    (function () {
        $scope.datalayer = datalayer;
        $scope.dldata = datalayer.dldata;
        $scope.userdatalayer = userdatalayer;
        $scope.dldata = userdatalayer.dldata;
        var id = userdatalayer.dldata.hotelData.Hotel.Id;
        var date = moment().format('YYYY-MM-DD');
        $scope.monthDate = moment().format('YYYY-MMMM');
        $scope.Todaydate = date;
        datalayer.dailyreport(date, id).then(function (report) {
            $scope.count = report.Report.count;
            $scope.request = report.Report.totalRequest;
            $scope.countBedtype = report.Report.RoomType;
            $scope.findRoomChart(report.Report.RoomType);
        });

        datalayer.monthlyreport(date, id).then(function (report) {
            $scope.monthcount = report.Report.count;
            $scope.monthrequest = report.Report.totalRequest;
            $scope.monthcountBedtype = report.Report.RoomType;
            $scope.monthfindRoomChart(report.Report.RoomType);
        });

        datalayer.getRoomTypes(id).then(function (data) {
            $scope.getTypeRoom = data;
        });
        $scope.datepicker = {
            dailyDate: { label: 'Select Date', type: 'date', defaultDate: 'today' }
        };
        $scope.Monthdatepicker = {
            MonthDate: { label: 'Select Date', template: "MonthPicker", type: 'date', defaultDate: 'today' }
        };
    })();
    $scope.dailyDateClick=function(dailyDateReceive) {
        var id = userdatalayer.dldata.hotelData.Hotel.Id;
        var date = moment(dailyDateReceive).format('YYYY-MM-DD');
        datalayer.dailyreport(date, id).then(function (report) {
            $scope.count = report.Report.count;
            $scope.request = report.Report.totalRequest;
            $scope.countBedtype = report.Report.RoomType;
            $scope.findRoomChart(report.Report.RoomType);
        });
    }
    $scope.monthlyDateClick = function (monthDateReceive) {
        var id = userdatalayer.dldata.hotelData.Hotel.Id;
        var date = moment(monthDateReceive).format('YYYY-MM-DD');
        datalayer.monthlyreport(date, id).then(function (report) {
            $scope.monthcount = report.Report.count;
            $scope.monthrequest = report.Report.totalRequest;
            $scope.monthcountBedtype = report.Report.RoomType;
            $scope.monthfindRoomChart(report.Report.RoomType);
        });
    }
    $scope.findRoomChart = function (roomTypes) {
        $scope.allTotal = 0;
        $scope.dailyTypeCatagoryArray = [];
        _.forEach(roomTypes, function (getTypeCatagory) {
            _.forEach(getTypeCatagory.Rooms, function (getTarrif) {
                var objs = {
                    BedType: getTypeCatagory.BedType,
                    RoomCategory: getTypeCatagory.RoomCategory,
                    RoomTariff:getTarrif.RoomTariff
                }
                var differentRooms = _.find($scope.dailyTypeCatagoryArray, function (object) {
                    if (object.BedType === getTypeCatagory.BedType && object.RoomCategory === getTypeCatagory.RoomCategory) {
                        return object;
                    }
                });
                if (angular.isUndefined(differentRooms)) {
                    $scope.dailyTypeCatagoryArray.push(objs);
                }
            });
           });
        $scope.bedTypeList = [];
        _.forEach($scope.dailyTypeCatagoryArray, function (item) {
            var countList = _.filter(roomTypes, function (room) {
                return room.BedType === item.BedType && room.RoomCategory === item.RoomCategory;
            });
            var bedTypes = { BedType: item.BedType,RoomTariff:item.RoomTariff,Total: item.RoomTariff * countList.length, RoomCategory: item.RoomCategory, Count: countList == undefined ? 0 : countList.length };
            $scope.bedTypeList.push(bedTypes);
           $scope.allTotal = $scope.allTotal + bedTypes.Total;
           
        });

    };

    $scope.monthfindRoomChart = function (roomType) {
        $scope.monthallTotal = 0;
        $scope.tempTypeCatagoryArray = [];
        $scope.TypeCatagoryArray = [];
        _.forEach(roomType, function (getTypeCatagory) {
            _.forEach(getTypeCatagory.Rooms, function(getTarrif) {
                var obj = {
                    BedType: getTypeCatagory.BedType,
                    RoomCategory: getTypeCatagory.RoomCategory,
                    RoomTariff: getTarrif.RoomTariff
                }
                var differentRoom = _.find($scope.tempTypeCatagoryArray, function(object) {
                    if (object.BedType === getTypeCatagory.BedType && object.RoomCategory === getTypeCatagory.RoomCategory) {
                        return object;
                    }
                });
                if (angular.isUndefined(differentRoom)) {
                    $scope.tempTypeCatagoryArray.push(obj);
                }

            });
        });
        $scope.monthbedTypeList = [];
        _.forEach($scope.tempTypeCatagoryArray, function (item) {
            var countLists = _.filter(roomType, function (room) {
                return room.BedType === item.BedType && room.RoomCategory === item.RoomCategory;
            });
            var bedType = { BedType: item.BedType, RoomTariff: item.RoomTariff, Total: item.RoomTariff * countLists.length, RoomCategory: item.RoomCategory, Count: countLists == undefined ? 0 : countLists.length };
            $scope.monthbedTypeList.push(bedType);
            $scope.monthallTotal = $scope.monthallTotal + bedType.Total;
        });

    };
}]);