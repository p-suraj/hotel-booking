﻿
//Datalayer
hotelapp.factory("hotelDatalayer", ["Restangular", "$csnotify", function (rest, $csnotify) {
    var api = rest.all("HotelApi");
    var dldata = {};
  
    dldata.hotelLists = [];
    dldata.hotelsaveddata = [];
    var gethotel = function () {
        api.customGET("GetHotels").then(function (data) {
            dldata.hotelLists = data;
            console.log(dldata.hotelLists);
        });
    };
    var savehotel = function (hotelobj) {
        api.customPOST(hotelobj, "SaveHotel").then(function (data) {
            $csnotify.success("saved","Hotel saved successfully");
            dldata.hotelsaveddata = data;
        });
    };

    return {
        dldata:dldata,
        gethotel: gethotel,
        savehotel: savehotel
    };
}]);
//===================================================hotel controller==================================================

hotelapp.controller("hotel1Controller", ["$scope", "$csnotify", "hotelDatalayer", function ($scope, $csnotify, datalayer) {
    //var gethotel = function() {};

    (function () {
        $scope.datalayer = datalayer;
        $scope.dldata = datalayer.dldata;

        $scope.addhotels = {};
        $scope.recdetails = [];
        $scope.hotelCities = ['Aurangabad', 'Bandra(Mumbai Suburban district)', 'Nagpur', 'Pune',
    ' Akola', 'Chandrapur', ' Jalgaon', 'Parbhani', 'Solapur', 'Thane', 'Latur',
    'Mumbai-City', 'Buldhana', 'Dhule', ' Kolhpur', 'Nanded', ' Raigad',
    'Amravati', 'Nashik', 'Wardha', ' Ahmednagar', 'Beed', 'Bhandara', 'Gadchiroli', 'Jalna',
    'Osmanabad', 'Ratnagiri', 'Sangli', 'Satara', 'Sindudurg', 'Yavatmal', 'Nandurbar', 'ashim',
    'Gondia', 'Hingoli'];
        $scope.addhl = false;
        $scope.count = 0;


        //  $scope.hotellist = $scope.dldata.list;
        $scope.hotel = {
            hotelname: { label: "Hotel Name", type: "text", required: true },
            hotelcity: { label: "City", type: "enum", valueList: $scope.hotelCities, required: true }
        };
    })();

    $scope.hoteladds = function(addhtl) {
        debugger;
        datalayer.savehotel(addhtl);
        $scope.addhotels = {};


        //var checkroom = _.find($scope.recdetails, function (rmcheck) {
        //    console.log("checking room id: ", rmcheck.rid);
        //    if (rmcheck.Name === addhtl.Name) {
        //        $csnotify.error("Error", "hotel added already");
        //        return rmcheck;

        //    }


        //});
        //if (angular.isUndefined(checkroom)) {
        //    $scope.recdetails.push(angular.copy(addhtl));
        //    $scope.count++;
        //    $scope.addhl = true;
        //    $scope.addhotels.Name = {};
        //    $scope.addhotels.City = {};
        //}

    };

}]);

//============================================================ room controller  ============================================================
hotelapp.controller("roomController", ["$scope", "$csnotify", function ($scope, $csnotify) {


    //=======================room master=========================================
    //==============room type detail==============================================
    $scope.addroomtyp = {};
    $scope.roomtyp = {};
    $scope.addshow = false;
    $scope.addroomarray = [];
    $scope.addroomdtls = function (addroomtyp) {
        debugger;
        $scope.addroomarray.push(angular.copy(addroomtyp));
        $scope.addshow = true;
        $scope.addroomtyp.addrhotelnm = {};
        $scope.addroomtyp.addrcategory = {};
        $scope.addroomtyp.addrbedtype = {};
    };

    $scope.gettype = function (obj) {
        $scope.getnewarray = [];
        _.forEach($scope.addroomarray, function (row) {
            if (row.addrhotelnm === obj.rhotelnm) {

                var list = row.addrcategory + '(' + row.addrbedtype + ')';

                $scope.getnewarray.push(angular.copy(list));
            }
        });
    };
    $scope.roomcatg = ["Standard", "Delux", "Suite"];
    $scope.roombed = ["2 bed", "3 bed", "5 bed", "Hall"];
    $scope.roomtypedetail = {
        roomHotelname: { label: "Hotel Name", type: "select", textField: "addrhotelnm", valueField: "addrhotelnm", valueList: $scope.addroomarray, required: "true" },
        roomCategory: { label: "Room type", type: "enum", required: "true" },
        roomRoomno: { label: "Room No", type: "number", required: "true" },
        roomRoomtarrif: { label: "Room Tarrif", type: "number", required: "true" }

    };
    $scope.typeshow = false;
    $scope.roomtypearray = [];
    //$scope.roomdtls = function (roomtyp) {
    //    debugger;
    //    $scope.roomtypearray.push(angular.copy(roomtyp));
    //    $scope.typeshow = true;
    //    $scope.roomtyp.rhotelnm = {};
    //    $scope.roomtyp.rcategory = {};
    //    $scope.roomtyp.rroomno = {};
    //};
    //==========================room add=======================================


    $scope.addroomtypedetail = {
        addroomHotelname: { label: "Hotel Name", type: "select", textField: "Name", valueField: "row", valueList: $scope.recdetails, required: "true" },
        addroomCategory: { label: "Category", type: "text", required: "true" },
        addroomBedtype: { label: "Bed Type", type: "text", required: "true" }
    };

}]);


