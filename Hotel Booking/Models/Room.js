﻿hotelapp.factory("RoomDataLayer", [
    "Restangular", "$csnotify", function (rest, $csnotify) {
        var api = rest.all("RoomApi");
        var hotelApi = rest.all("HotelApi");
        var roomTypeApi = rest.all("RoomTypeApi");
        var dldata = {};
        dldata.RoomSave = [];
        dldata.RoomList = [];
        dldata.HotelLists = [];
        dldata.RoomTypeLists = [];
        dldata.hotelRoomType = [];
        dldata.list = [];

        var getRoomTypes = function (id) {
           return  roomTypeApi.customGET("GetRoomTypes", { id: id }).then(function (data) {
                dldata.RoomTypeLists = data;
               console.log(dldata.hotelLists);
               return data;
            });
        };
        var getHotel = function () {
          return  hotelApi.customGET("GetHotels").then(function (data) {
              dldata.HotelLists = data;
              return data;
          });
        };

        var roomSave = function (roomobj) {
            debugger;
            api.customPOST(roomobj, "SaveRoom").then(function (data) {
                dldata.RoomSave = data;
                $csnotify.success("saved", "RoomType saved successfully");
              
            });
        };

        //var hotelType = function (room) {

        //    return api.customGET("GetTypesBasedOnHotel", { id: room.Hotel.Id }).then(function (data) {
        //        dldata.hotelRoomType = data;
        //        dldata.list = [];
        //        _.forEach(data, function (item) {
        //            if (item.Hotel.Id === room.Hotel.Id) {
        //                var x = item.RoomCategory + '(' + item.BedType + ')';
        //                var obj = {
        //                    row: item,
        //                    Name: x
        //                };
        //                dldata.list.push(obj);

        //            }
        //        });
        //    });
        //};

        var getrooms = function () {
            api.customGET("GetRooms").then(function (data) {
                dldata.RoomList = data;
                console.log(dldata.hotelLists);
            });
        };
        return {
            dldata: dldata,
            roomSave: roomSave,
            getrooms: getrooms,
            getHotel: getHotel,
            getRoomTypes: getRoomTypes
            //hotelType: hotelType
        };
    }]);

hotelapp.controller("RoomController", ["$scope", "$csnotify", "RoomDataLayer", "userDetailDatalayer", function ($scope, $csnotify, datalayer, roomdataLayer) {
    (function () {
        $scope.roomData = [];
        $scope.arrayname = [];
        $scope.roomtyp = {};
        $scope.list = [];
        $scope.datalayer = datalayer;
        $scope.dldata = datalayer.dldata;
       debugger;
        datalayer.getHotel().then(function(data) {
            $scope.list = data;
        });
       //var id = roomdataLayer.dldata.hotelData.Id;
       // datalayer.getRoomTypes(id).then(function(data) {
       //     $scope.roomData = data;
       //     _.forEach($scope.roomData, function (categoryBed) {
       //         var x = categoryBed.RoomCategory + '(' + categoryBed.BedType + ')';
       //         var obj = {
       //             Value: categoryBed,
       //             Names: x
       //         };
       //         $scope.arrayname.push(obj);
       //         //  $scope.arrayname.push(categoryBed.Id);
       //     });
       // });
       
      })();
    $scope.addroomdetail = {
        Hotelnames: { label: "Hotel Name", type: "select",textField: "Name",valuefield: "Name"},
        addroomType: { label: "RoomType", type: "select", textField: "Names", valueField: "Value", required: "true" },
        addroomNo: { label: "Room Number", type: "text", required: "true" },
        addroomtariff: { label: "Room Tariff", type: "text", required: "false" }
    };
   $scope.AddRoomDetails = function (addroomobj) {
        debugger;
        //var obj = { Name: roomdataLayer.dldata.hotelData.Name, addroomobj: addroomobj };
        datalayer.roomSave(addroomobj);
        $scope.roomtyp = {};
   };
   $scope.getRoomType = function (obj) {
       debugger;
       var Id = obj.Id;
       datalayer.getRoomTypes(Id).then(function (data) {
           $scope.roomDatas = data;
           $scope.arrayname = [];
           if ($scope.roomDatas!=="") {
               _.forEach($scope.roomDatas, function (categoryBed) {
                   var x = categoryBed.RoomCategory + '(' + categoryBed.BedType + ')';
                   var roomobj = {
                       Value: categoryBed,
                       Names: x
                   };
                   $scope.arrayname.push(roomobj);
                   //  $scope.arrayname.push(categoryBed.Id);
               });
           }
          
       });

   };
}]);
