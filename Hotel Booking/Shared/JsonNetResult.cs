﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using DataLayers.NhSetup;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Hotel_Booking.Shared
{

    public class JsonNetResult : JsonResult
    {
        private JsonSerializerSettings Settings { get; set; }

        public JsonNetResult()
        {
            Settings = new JsonSerializerSettings();
            JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            InitSettings(Settings);
        }

        public static JsonSerializerSettings InitSettings(JsonSerializerSettings settings)
        {
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Formatting = Formatting.Indented;
           // settings.Converters.Add(new NhProxyJsonConverter());
            settings.Converters.Add(new StringEnumConverter());
            return settings;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("JSON GET is not allowed");
            }

            // set json response
            var response = context.HttpContext.Response;
            response.ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType;

            // set utf-8 encoding
            if (ContentEncoding == null)
            {
                response.ContentEncoding = Encoding.UTF8;
            }

            // data is must for response
            if (Data == null)
            {
                return;
            }

            // create serializer and return the response
            var scriptSerializer = JsonSerializer.Create(Settings);

            using (var sw = new StringWriter())
            {
                scriptSerializer.Serialize(sw, Data);
                response.Write(sw.ToString());
            }
        }
    }
}

