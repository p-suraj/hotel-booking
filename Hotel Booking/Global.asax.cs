﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DataLayers.NhSetup;
using Hotel_Booking.Shared;
using Hotel_Booking.WebApi;

namespace Hotel_Booking
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly NhSetup _obj = new NhSetup();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            _obj.Setup();

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy =
             IncludeErrorDetailPolicy.Always;
            JsonNetResult.InitSettings(GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings);
            GlobalConfiguration.Configuration.Formatters.Insert(0, new TextMediaTypeFormatter());
        }
    }
}