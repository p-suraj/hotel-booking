using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer;
using DataLayer.SessionMgr;
using DataLayers.Domain;
using DataLayers.SharedUtility;
using Newtonsoft.Json.Linq;
using NHibernate.Linq;

namespace Hotel_Booking.WebApi
{
    public class UsersApiController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage ValidateUser(JObject loginInfo)
        {
            var session = SessionManager.GetNewSession();
            var user = session.QueryOver<Users>()
                .Where(x => x.UserName == loginInfo["UserName"].ToString())
                .Where(x => x.Password == loginInfo["Password"].ToString())
                .Fetch(x=>x.Hotel).Eager
                .SingleOrDefault();

            if (user == null)
            {
                var statusFalse = new Status() { Statuses = false };
                var modelFalse = new UserViewModel() { Hotel = null, Status = statusFalse };
                return Request.CreateResponse(HttpStatusCode.OK, modelFalse);
            }

            var status = new Status() { Statuses = true };
            var model = new UserViewModel() { Hotel = user, Status = status };

            var response = Request.CreateResponse(HttpStatusCode.Accepted, model);

            return response;

        }

        [HttpPost]
        public HttpResponseMessage LoginUser(LoginUser loginUser)
        {
            var session = SessionManager.GetNewSession();
            var User = session.Query<Users>()
                .Where(x => x.UserName == loginUser.UserName)
                .Where(x => x.Password == loginUser.Password)
                .Fetch(x => x.Hotel).ThenFetch(x=>x.Users)
                .SingleOrDefault();
          if (User==null)
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable,"Wrong UserName & Password");
            }
            if (User.Hotel== null)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, User);
            }
            User.Hotel.Bookings = null;
            User.Hotel.RoomTypes = null;
            //User.Hotel = null;
            //hotel.Hotel.RoomTypes = null;
            return Request.CreateResponse(HttpStatusCode.Accepted, User);
        }

        [HttpGet]
        public HttpResponseMessage FetchBookingInfo(int hotelId)
        {
            var session = SessionManager.GetNewSession();

            var bookings = session.QueryOver<Booking>()
                .Where(x => x.Hotel.Id == hotelId)
                .List<Booking>();

            var listOfBooking = new JArray();

            foreach (var booking in bookings)
            {
                var temp = new JObject
                {
                    {"Id", booking.Id},
                    {"CustomerName",booking.CustomerName},
                    {"City", booking.City},
                    {"UpdatedOn",booking.UpdatetedOn},
                    {"HotelId", booking.Hotel.Id },
                    {"HotelName",booking.Hotel.Name},
                    {"HotelCity",booking.Hotel.City}
                           
                   
                };
                listOfBooking.Add(temp);
            }

            return Request.CreateResponse(HttpStatusCode.OK, listOfBooking);
        }


        [HttpPost]
        public HttpResponseMessage SaveUsers(Users model)
        {
            var response = new JObject();
            try
            {
                if (model == null)
                {
                    response.Add("Status", "Invalid Information Provided");
                    return Request.CreateResponse(HttpStatusCode.Accepted, response);
                }

                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.SaveOrUpdate(model);
                        tx.Commit();
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }
        }


        //[HttpPost]
        //public HttpResponseMessage GetBookingData()
        //{
        //    var session = SessionManager.GetNewSession();
        //    Mapper.CreateMap<Users, UserDTO>();
        //    Users list = (from u in session.Query<Users>()
        //                  select u).FirstOrDefault();
        //    UserDTO dto = Mapper.Map<Users, UserDTO>(list);
        //    return Request.CreateResponse(HttpStatusCode.OK, dto);
        //}
    }
}