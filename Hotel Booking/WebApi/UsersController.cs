﻿using DataLayer;
using DataLayers.Domain;

namespace Hotel_Booking.WebApi
{
    public class UserViewModel
    {
        public Users Hotel { get; set; }
        public Status Status { get; set; }
    }

    public class Status
    {
        public bool Statuses { get; set; }
    }

    public class LoginUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }

    }

}
