﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using DataLayer;
using DataLayer.SessionMgr;
using DataLayers.Domain;
using DataLayers.SharedUtility;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using NHibernate.Dialect.Function;
using NHibernate.Hql.Ast.ANTLR;
using NHibernate.Linq;
using NHibernate.Mapping;

namespace Hotel_Booking.WebApi
{
    public class BookingApiController : ApiController
    {
        //[HttpGet]
        //public HttpResponseMessage GetLatestBooking(string timeStamp)
        //{
        //    List<Booking> data;
        //    using (var session = SessionManager.GetNewSession())
        //    {
        //        data = session.Query<Booking>()
        //            .Where(x => x.UpdatetedOn > DateTime.Parse(timeStamp))
        //            .ToList();
        //        if (data.Count <= 0)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Invalid Information Provided");
        //        }
        //    }
        //    return Request.CreateResponse(HttpStatusCode.Accepted, data);
        //}
        [HttpGet]
        public HttpResponseMessage GetBookings(int id)
        {
            var session = SessionManager.GetNewSession();
            var bookings = session.Query<RoomRelations>()
                .Fetch(x => x.Booking)
                .Fetch(x => x.Room)
                .ThenFetch(x => x.RoomType)

                .Where(x => x.Booking.Hotel.Id == id)
                .ToList();
            if (bookings.Count <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Invalid Information Provided");
            }
            var dtoBooking = new DtoBooking();
            var bookingList = new List<Booking>();
            foreach (var book in bookings)
            {
                var b = new Booking();
                b.RoomRelations = null;
                b.Hotel = null;
                b = book.Booking;
                bookingList.Add(b);
            }
            dtoBooking.Bookings = bookingList;
            return Request.CreateResponse(HttpStatusCode.Accepted, dtoBooking);
        }

        [HttpPost]
        public HttpResponseMessage SaveBooking(BookingViewModel model)
        {
            var response = new JObject();
            try
            {
                if (model == null)
                {
                    response.Add("Status", "Invalid Information Provided");
                    return Request.CreateResponse(HttpStatusCode.Accepted, response);
                }

                var rmList = model.Rooms
                    .Select(iteam => new RoomRelations {Booking = model.Booking, Room = iteam})
                    .ToList();
                var booking = SessionManager.GetNewSession().Query<Booking>().ToList();

                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        model.Booking.RoomRelations = rmList;
                        //model.Booking.Hotel = session.Load<Hotel>(id);
                        session.SaveOrUpdate(model.Booking);
                        tx.Commit();
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {
                response.Add("Status", "Error");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, response);
            }
        }

        [HttpPost]
        public HttpResponseMessage BookingUpdate(DtoRoomRelation model)
        {
            var response = new JObject();
            try
            {
                if (model == null)
                {
                    response.Add("Status", "Invalid Information Provided");
                    return Request.CreateResponse(HttpStatusCode.Accepted, response);
                }

                var roomRelation = SessionManager.GetNewSession().Query<RoomRelations>()

                    .SingleOrDefault(x => x.Id == model.Id);



                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        if (roomRelation != null)
                        {
                            roomRelation.Room = model.Room;
                            roomRelation.Booking = model.Booking;
                            roomRelation.Status = model.Status;
                            session.SaveOrUpdate(roomRelation.Booking);
                            session.Update(roomRelation);
                        }
                        tx.Commit();
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {
                response.Add("Status", "Error");
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, response);
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateStatus(UpdateStatus status)
        {
            var response = new JObject();
            using (var session = SessionManager.GetNewSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    if (status.Status == HotelEnums.BookingStatus.Canceled)
                    {
                        var booking = session.Query<RoomRelations>()
                            .Where(x => x.Room.Id == status.RoomId)
                            .SingleOrDefault(x => x.Booking.Id == status.BookId);
                        session.Delete(booking);
                    }
                    else
                    {
                        var data = session.Query<RoomRelations>()
                            .Where(x => x.Room.Id == status.RoomId)
                            .SingleOrDefault(x => x.Booking.Id == status.BookId);
                        if (data != null)
                        {
                            data.Status = status.Status;

                            session.Update(data);
                        }
                    }

                    tx.Commit();
                }
            }
            response.Add("Status", "Successful");
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage GetStatusEnum()
        {
            var data = Enum.GetNames(typeof (HotelEnums.BookingStatus));
            return Request.CreateResponse(HttpStatusCode.Accepted, data);
        }

        public IEnumerable<string> GetStatuses()
        {
            var data = Enum.GetNames(typeof (HotelEnums.BookingStatus));

            return data;
        }

        [HttpGet]
        public HttpResponseMessage GetTodayBooking(DateTime date, int Id)
        {
            var session = SessionManager.GetNewSession();

            var data = session.Query<RoomRelations>()
                .Fetch(x => x.Room)
                .ThenFetch(x => x.RoomType)
                .Fetch(x => x.Booking)
                .Where(x => x.Booking.Hotel.Id == Id)
                .Where(x => x.Booking.CheckInDate.Date == date.Date)

                .ToList();

            var roomRelationList = new DtoBooking();

            roomRelationList.RoomRelations = data;

            return Request.CreateResponse(HttpStatusCode.OK, roomRelationList);
        }

        [HttpGet]
        public HttpResponseMessage GetMonthlyBooking(DateTime date)
        {
            var session = SessionManager.GetNewSession();

            var data =
                session.Query<Booking>()
                    .Where(x => x.CheckInDate.Month == date.Month)
                    .Where(x => x.CheckInDate.Year == date.Year)
                    .ToList();
            var booking = new BookingDto() {Booking = data};

            return Request.CreateResponse(HttpStatusCode.OK, booking);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteBooking(Booking model)
        {
            var response = new JObject();
            try
            {
                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.Delete(model);
                        tx.Commit();
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetAllBooking(int id)
        {
            var session = SessionManager.GetNewSession();
            var booking = session.Query<RoomRelations>()
                .Fetch(x => x.Booking)
                .Fetch(x => x.Room)
                .Where(x => x.Booking.Hotel.Id == id)
                .OrderByDescending(x=>x.Booking.CheckInDate)
                .ToList();
            var roomRelationList = new GetAllBookDto();

            roomRelationList.RoomRelations = booking;
            return Request.CreateResponse(HttpStatusCode.Accepted, roomRelationList);
        }

        [HttpGet]
        public HttpResponseMessage Reports(DateTime date, int Id)
        {
            var session = SessionManager.GetNewSession();
            var roomtype = session.Query<RoomRelations>()
                .Fetch(x => x.Room)
                .ThenFetch(x => x.RoomType)
                .Where(x => x.Room.RoomType.Hotel.Id == Id)
                .Where(x => x.Booking.CheckInDate.Date == date.Date)
                .Where(x => x.Status == HotelEnums.BookingStatus.CheckIn || x.Status == HotelEnums.BookingStatus.CheckOut)
                .ToList();

            var CheckInCount = session.Query<RoomRelations>()
                .Where(x => x.Booking.CheckInDate.Date == date.Date)
                .Where(x => x.Status == HotelEnums.BookingStatus.CheckIn || x.Status == HotelEnums.BookingStatus.CheckOut)
                .ToList();

           var tariff = session.Query<RoomRelations>()
                .Fetch(x => x.Room)
                .ThenFetch(x=>x.RoomType)
                .Where(x => x.Booking.CheckInDate.Date == date.Date)
                .Where(x=>x.Status==HotelEnums.BookingStatus.CheckIn ||x.Status==HotelEnums.BookingStatus.CheckOut)
                .ToList();

            var requests = session.Query<RoomRelations>()
                .Where(x => x.Booking.CheckInDate.Date == date.Date)
                .Fetch(x => x.Booking)
                .ToList();
            var totalRequest = requests.Count;
            var CheckInCheckOutCount = CheckInCount.Count;
            var dtr = new List<RoomType>();
            //var dr = new List<Room>();
            foreach (var relationse in roomtype)
            {
                dtr.Add(relationse.Room.RoomType);
            }
           
            var demo = new Demo() {RoomType = dtr};
            var rp = new List<ReportsDto>();
            foreach (var reportsDto in tariff)
            {
                var reports = new ReportsDto();
                reports.roomtariff =Convert.ToInt32(reportsDto.Room.RoomTariff);
                reports.category = reportsDto.Room.RoomType.RoomCategory;
                reports.BedType = reportsDto.Room.RoomType.BedType;
                rp.Add(reports);
            }
            
            demo.ReportsDtos = rp;
            demo.totalRequest = totalRequest;
            demo.count = CheckInCheckOutCount;
            
            Dumy nw = new Dumy() {Report = demo};
            return Request.CreateResponse(HttpStatusCode.OK, nw);
        }

        [HttpGet]
        public HttpResponseMessage MonthlyReports(DateTime date, int Id)
        {
            var session = SessionManager.GetNewSession();
            
            var roomtype = session.Query<RoomRelations>()
                .Where(x => x.Room.RoomType.Hotel.Id == Id)
                .Where(x => x.Booking.CheckInDate.Month == date.Month)
                .Where(x => x.Booking.CheckInDate.Year == date.Year)
                .Where(x => x.Status == HotelEnums.BookingStatus.CheckIn || x.Status == HotelEnums.BookingStatus.CheckOut)
                .Fetch(x => x.Room)
                .ThenFetch(x => x.RoomType)
                .ToList();
            var CheckInCount = session.Query<RoomRelations>()
                .Where(x => x.Booking.CheckInDate.Month == date.Month)
                .Where(x => x.Booking.CheckInDate.Year == date.Year)
                .Where(x => x.Status == HotelEnums.BookingStatus.CheckIn || x.Status == HotelEnums.BookingStatus.CheckOut)
                .ToList();
            var CheckInCheckOutCount = CheckInCount.Count;

            var tariff = session.Query<RoomRelations>()
                .Fetch(x => x.Room)
                .ThenFetch(x => x.RoomType)
                .Where(x => x.Booking.CheckInDate.Month == date.Month)
                .Where(x => x.Booking.CheckInDate.Year == date.Year)
                .Where(x => x.Status == HotelEnums.BookingStatus.CheckIn || x.Status == HotelEnums.BookingStatus.CheckOut)
                .ToList();

            var requests = session.Query<RoomRelations>()
               .Where(x => x.Booking.CheckInDate.Month == date.Month)
                .Where(x => x.Booking.CheckInDate.Year == date.Year)
               .Fetch(x => x.Booking)
               .ToList();
            var totalRequest = requests.Count;

            //Fvar roomRelationList = new ReportsDto();
            //var totalRequest = roomtype.Count();
            //roomRelationList.count = CheckInCheckOutCount;
            var rt = new List<RoomType>();
            
            foreach (var relationse in roomtype)
            {
                rt.Add(relationse.Room.RoomType);
            }
            var demo = new Demo() { RoomType = rt };
            var rp = new List<ReportsDto>();
            foreach (var reportsDto in tariff)
            {
                var reports = new ReportsDto();
                reports.roomtariff = Convert.ToInt32(reportsDto.Room.RoomTariff);
                reports.category = reportsDto.Room.RoomType.RoomCategory;
                reports.BedType = reportsDto.Room.RoomType.BedType;
                rp.Add(reports);
            }

            demo.ReportsDtos = rp;
            demo.totalRequest = totalRequest;
            demo.count = CheckInCheckOutCount;
            Dumy nw = new Dumy() { Report = demo };
            return Request.CreateResponse(HttpStatusCode.OK,nw);
            
        }

    }

    public class BookingViewModel
    {
        public Booking Booking { get; set; }
        public List<Room> Rooms { get; set; }

    }

    public class DtoBooking
    {
        public Booking Booking { get; set; }
        public Room Room { get; set; }
        public RoomType RoomType { get; set; }
        public List<Booking> Bookings { get; set; }
        public List<RoomRelations> RoomRelations { get; set; }
    }

    public class BookingDto
    {
        public List<Booking> Booking { get; set; }
    }

    public class UpdateStatus
    {
        public int RoomId { get; set; }
        public int BookId { get; set; }
        public HotelEnums.BookingStatus Status { get; set; }
    }

    public class DtoRoomRelation
    {
        public int Id { get; set; }
        public Booking Booking { get; set; }
        public Room Room { get; set; }
        public HotelEnums.BookingStatus Status;
    }

    public class GetAllBookDto
    {
        public string status { get; set; }
        public Booking Booking { get; set; }
        public Room Room { get; set; }
        public List<RoomRelations> RoomRelations { get; set; }

    }

    //public class ReportsDto
    //{
    //    public Booking Booking { get; set; }
    //    public List<RoomType> RoomType { get; set; }
    //    public List<Room> Rooms { get; set; }
    //    public int count { get; set; }
    //    public int totalRequest { get; set; }
    //}

    public class Demo
    {
        public List<Room> Room { get; set; }
        public List<RoomType> RoomType { get; set; }
        public int totalRequest { get; set; }
        public int count { get; set; }
        //public int tariff { get; set; }
        public List<ReportsDto> ReportsDtos { get; set; }
    }

    public class ReportsDto
    {
        public string category { get; set; }
        public string BedType { get; set; }
        public decimal roomtariff { get; set; }
    }
    public class Dumy
    {
        public Demo Report { get; set; }
    }
}
