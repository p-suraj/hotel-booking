﻿using DataLayer;
using DataLayers.SharedUtility;

namespace Hotel_Booking.WebApi
{
    public class UserDTO
    {
        public uint Id;
        public string Name;
        public HotelEnums.UserType UserType;
        public double ContactNo;
        public string EmailId;
        public HotelDTO Hotel;
        public string UserName;
        public string Password;
    }

    public class HotelDTO
    {
        public uint Id;
        public string Name;
        public string City;
    }

}