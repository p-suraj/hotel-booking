﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer;
using DataLayer.SessionMgr;
using DataLayers.Domain;
using Newtonsoft.Json.Linq;
using NHibernate.Linq;
using NHibernate.SqlCommand;

namespace Hotel_Booking.WebApi
{
    public class HotelApiController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage SaveHotel(Hotel model)
        {
            var response = new JObject();
            try
            {
                if (model == null)
                {
                    response.Add("Status", "Invalid Information Provided");
                    return Request.CreateResponse(HttpStatusCode.Accepted, response);
                }

                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.SaveOrUpdate(model);
                        tx.Commit();
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }

        }

        [HttpGet]
        public HttpResponseMessage GetHotels()
        {
            var session = SessionManager.GetNewSession();
            var hotels = session.QueryOver<Hotel>().List<Hotel>();
            if (hotels.Count <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Hotels not found");
            }
            var hotelList = new List<Hotel>();
            foreach (var iteam in hotels)
            {
                iteam.RoomTypes = null;
                iteam.Bookings = null;
                iteam.Users = null;
                hotelList.Add(iteam);
            }
            return Request.CreateResponse(HttpStatusCode.OK, hotelList);
        }
        [HttpGet]
        public HttpResponseMessage GetHotelById(int Id)
        {
            var session = SessionManager.GetNewSession();
            var hotel = session.QueryOver<Hotel>()
                .Where(x => x.Id == Id)
                .SingleOrDefault();
            if (hotel == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Hotels not found");
            }
            return Request.CreateResponse(HttpStatusCode.OK, hotel);
        }

        [HttpGet]
        public HttpResponseMessage GetHotelAllDetailsById(int Id)
        {
            var session = SessionManager.GetNewSession();
            Hotel hotel = null;
            RoomType roomType = null;
            Room room = null;
          
            var rt = session.Query<RoomType>()
                .Fetch(x => x.Rooms)
                .Where(x => x.Hotel.Id == Id)
                .ToList();
            
            var ht = new singleHotel();
            ht.RoomType = (List<RoomType>)rt;
            foreach (var type in rt)
            {
                type.Hotel = null;

            }
            var listOfRooms = rt.SelectMany(x => x.Rooms).ToList();
            

            if (rt.Count <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Hotels not found");
            }
           


            return Request.CreateResponse(HttpStatusCode.OK, ht);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteHotel(Hotel model)
        {
            var response = new JObject();
            try
            {
                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.Delete(model);
                        tx.Commit();
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }

        }
    }

    public class singleHotel
    {
        public List<RoomType> RoomType { get; set; }
    }
}
