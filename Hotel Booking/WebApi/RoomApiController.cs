﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using DataLayer;
using DataLayer.SessionMgr;
using DataLayers.Domain;
using Newtonsoft.Json.Linq;

namespace Hotel_Booking.WebApi
{
    public class RoomApiController : ApiController
    {
        public static DateTime IsMasterUpdate { get; set; }

        [HttpPost]
        public HttpResponseMessage SaveRoom(Room model)
        {
            
            var response = new JObject();
            try
            {
                if (model == null)
                {
                    response.Add("Status", "Invalid Information Provided");
                    return Request.CreateResponse(HttpStatusCode.Accepted, response);
                }

                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.SaveOrUpdate(model);
                        tx.Commit();
                        IsMasterUpdate = DateTime.Now;
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetTypesBasedOnHotel(int id)
        {
            var session = SessionManager.GetNewSession();
           
                var data = session.QueryOver<RoomType>()
                    .Fetch(x=>x.Hotel).Eager
                    .Where(z => z.Hotel.Id == id)
                    .List<RoomType>();
           
                return Request.CreateResponse(HttpStatusCode.OK, data);
           

        }

        [HttpDelete]
        public HttpResponseMessage DeleteRoom(Room model)
        {
            var response = new JObject();
            try
            {
                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.Delete(model);
                        tx.Commit();
                        IsMasterUpdate = DateTime.Now;
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }

        }

        [HttpGet]
        public HttpResponseMessage CheckStatus()
        {
            return Request.CreateResponse(HttpStatusCode.Accepted, IsMasterUpdate);
        }

        [HttpGet]
        public HttpResponseMessage GetRooms()
        {
            var session = SessionManager.GetNewSession();
            var rooms = session.QueryOver<Room>().List<Room>();
            if (rooms.Count <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Hotels not found");
            }


            return Request.CreateResponse(HttpStatusCode.OK, rooms);
        }

    }
}
