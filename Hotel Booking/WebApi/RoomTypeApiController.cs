﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer;
using DataLayer.SessionMgr;
using Newtonsoft.Json.Linq;
using NHibernate.Linq;

namespace Hotel_Booking.WebApi
{
    public class RoomTypeApiController : ApiController
    {
        public static DateTime IsMasterUpdate { get; set; }
        [HttpPost]
        public HttpResponseMessage SaveRoomType(RoomType model)
        {
            var response = new JObject();
            try
            {
                if (model == null)
                {
                    response.Add("Status", "Invalid Information Provided");
                    return Request.CreateResponse(HttpStatusCode.Accepted, response);
                }

                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.SaveOrUpdate(model);
                        tx.Commit();
                        IsMasterUpdate = DateTime.Now;
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }
        }

        [HttpDelete]
        public HttpResponseMessage DeleteRoomType(RoomType model)
        {
            var response = new JObject();
            try
            {
                using (var session = SessionManager.GetNewSession())
                {
                    using (var tx = session.BeginTransaction())
                    {
                        session.Delete(model);
                        tx.Commit();
                        IsMasterUpdate = DateTime.Now;
                    }
                }
                response.Add("Status", "Successful");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception e)
            {

                return Request.CreateResponse(HttpStatusCode.Accepted, e.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetRoomTypes(int id)
        {
            var session = SessionManager.GetNewSession();
            var roomTypes = session.Query<RoomType>()

            .Where(x => x.Hotel.Id == id)

            .ToList();

            if (roomTypes.Count <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted);
            }
            var rtList = new List<RoomType>();
            foreach (var type in roomTypes)
            {
                type.Rooms = null;
                type.Hotel = null;
                rtList.Add(type);
            }
            return Request.CreateResponse(HttpStatusCode.OK, rtList);
        }

        [HttpGet]
        public HttpResponseMessage GetRoomsAndTypeByHotelId(int id)
        {
            var session = SessionManager.GetNewSession();
            var roomTypes = session.Query<RoomType>()
                .Fetch(x=>x.Rooms)
                .Where(x => x.Hotel.Id == id)
            .ToList();

            if (roomTypes.Count <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.Accepted, "RoomType not found");
            }
            var rtList = new List<RoomType>();
            foreach (var type in roomTypes)
            {
                foreach (var room in type.Rooms)
                {
                    room.RoomRelations = null;
                }
                
                type.Hotel = null;
                
                rtList.Add(type);
            }
            return Request.CreateResponse(HttpStatusCode.OK, rtList);
        }
        [HttpGet]
        public HttpResponseMessage CheckStatus()
        {
            return Request.CreateResponse(HttpStatusCode.Accepted, IsMasterUpdate);
        }
    }
}
