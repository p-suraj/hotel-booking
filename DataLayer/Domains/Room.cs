﻿using System.Collections.Generic;

namespace DataLayer
{
    public class Room :Entity
    {
        public virtual uint RoomNumber { get; set; }
        public virtual RoomType RoomType { get; set; }
        public virtual IList<Booking> Bookings { get; set; }
    }
}
