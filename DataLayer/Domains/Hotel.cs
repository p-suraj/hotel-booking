﻿using System.Collections.Generic;

namespace DataLayer
{
    public class Hotel :Entity
    {
        public virtual string Name { get; set; }
        public virtual string City { get; set; }

        public virtual IList<Users> Users { get; set; }

        public virtual IList<RoomType> RoomTypes { get; set; }
        public virtual IList<Booking> Bookings { get;set; }
    }
}
