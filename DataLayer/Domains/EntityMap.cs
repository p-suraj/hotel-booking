﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode.Impl;

namespace DataLayer
{
    public abstract class EntityMap<T> : ClassMapping<T> where T : Entity
    {
        public EntityMap()
        {
            Id(x => x.Id, map => map.Generator(Generators.Identity));
        }
    }
}
