﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public  class Booking :Entity
    {
        public virtual string CustomerName { get; set; }
        public virtual double MobileNumber { get; set; }
        public virtual  string City { get; set; }
        public virtual DateTime CheckInDate { get; set; }
        public virtual DateTime CheckInTime { get; set; }
        public virtual HotelEnums.BookingStatus Status { get; set; }
        public virtual Room Room { get; set; }
        public virtual RoomType RoomType { get; set; }
        public virtual Hotel Hotel { get; set; }

    }
}
