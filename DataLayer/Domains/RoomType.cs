﻿using System.Collections.Generic;

namespace DataLayer
{
    public class RoomType:Entity
    {
        public virtual string RoomCategory { get; set; }
        public virtual string BedType { get; set; }
        public virtual Hotel Hotel { get; set; }
        public virtual IList<Room> Rooms { get; set; }
        public virtual IList<Booking> Bookings { get; set; }
    }
}
