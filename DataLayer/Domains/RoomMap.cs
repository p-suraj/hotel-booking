﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataLayer
{
   public class RoomMap:EntityMap<Room>
    {
        public RoomMap()
        {
            Property(x=>x.RoomNumber);
            ManyToOne(x=>x.RoomType);
            Bag(x=>x.Bookings, map => { },colmap=>colmap.OneToMany());
        }
    }
}
