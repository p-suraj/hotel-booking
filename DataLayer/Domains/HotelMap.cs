﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataLayer
{
    public class HotelMap : EntityMap<Hotel>
    {
        public HotelMap()
        {
            Property(x => x.Name);
            Property(x => x.City);
            Bag(x => x.Users, map => { }, colmap => colmap.OneToMany());
            Bag(x => x.RoomTypes, map => { },colmap=> colmap.OneToMany());
            Bag(x => x.Bookings, map => { }, colmap => colmap.OneToMany());

        }
    }
}
