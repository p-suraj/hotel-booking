﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataLayer
{
   public class BookingMap:EntityMap<Booking>
    {
       public BookingMap()
       {
           Property(x=>x.CustomerName);
           Property(x=>x.City);
           Property(x=>x.CheckInTime);
           Property(x=>x.CheckInDate);
           Property(x=>x.MobileNumber);
           Property(x=>x.Status);
           ManyToOne(x=>x.Hotel, map => { });
           ManyToOne(x=>x.Room, map => { });
           ManyToOne(x=>x.RoomType, map => { });
       }
    }
}
