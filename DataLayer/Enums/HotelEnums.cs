﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Hql.Ast.ANTLR;

namespace DataLayer
{
    public static class HotelEnums
    {
        [Serializable]
        public enum UserType
        {
            HotelManager,
            Owner
        }


        [Serializable]
        public enum BookingStatus
        {
            Pending, 
            Confirmed,
            NotConfirmed

        }

    }
}
