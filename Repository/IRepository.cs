﻿using System;

namespace Repository
{
    public interface IRepository
    {
        void SaveCustomer();
        void UpdateCustomer();
        void DeleteCustomer();

    }

    class Repository : IRepository
    {
        public void SaveCustomer()
        {
            throw new NotImplementedException();
        }

        public void UpdateCustomer()
        {
            throw new NotImplementedException();
        }

        public void DeleteCustomer()
        {
            throw new NotImplementedException();
        }
    }
}
