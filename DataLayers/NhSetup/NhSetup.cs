﻿using System.Configuration;
using DataLayer.SessionMgr;
using DataLayers.SessionMgr;
using NHibernate.Tool.hbm2ddl;

namespace DataLayers.NhSetup
{
    public class NhSetup
    {
        public void Setup()
        {
          
            //var connectionString = ConfigurationManager.ConnectionStrings["HotelConnString"];
            var connectionString = SharedUtility.ColloSysParam.WebParams.ConnectionString;

               
            var obj = new NhInitParams
            {
                ConnectionString = connectionString,
                DbType = ConfiguredDbTypes.MsSql,
                IsWeb = false
            };

            SessionManager.InitNhibernate(obj);
            //MappingSetup.Setup(cfg);
            
        }

        public  void GenrateDb()
        {
            var cfg = SessionManager.GetNhConfiguration();
            var schema = new SchemaExport(cfg);
            schema.Create(false, true);
        }
    }
}
