﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;

namespace DataLayers.NhSetup
{
    public static class SessionExtensions
    {
        public static Boolean IsDirtyEntity(this ISession session, Object entity)
        {
            var sessionImpl = session.GetSessionImplementation();
            var persistenceContext = sessionImpl.PersistenceContext;
            var oldEntry = persistenceContext.GetEntry(entity);
            var className = oldEntry.EntityName;
            var persister = sessionImpl.Factory.GetEntityPersister(className);

            var oldState = oldEntry.LoadedState;
            if (oldState == null) return true;

            var currentState = persister.GetPropertyValues(entity, sessionImpl.EntityMode);
            var dirtyProps = persister.FindDirty(currentState, oldState, entity, sessionImpl);

            return ((dirtyProps != null) && (dirtyProps.Length > 0));
        }
    }
}
