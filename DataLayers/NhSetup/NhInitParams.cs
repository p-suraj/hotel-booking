﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using DataLayers.NhSetup;
using DataLayers.SessionMgr;

namespace DataLayer.SessionMgr
{
    public class NhInitParams
    {
        public NhInitParams()
        {
            DbType = ConfiguredDbTypes.MsSql;
            ConnectionString = new ConnectionStringSettings();
            IsWeb = false;
        }

        public ConfiguredDbTypes DbType { get; set; }

        public ConnectionStringSettings ConnectionString { get; set; }

        public bool IsWeb { get; set; }
    }
}
