﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NHibernate.Dialect;
using System.Threading.Tasks;

namespace NgSetup
{
    public class ExtendedMsSql2008Dialect : MsSql2008Dialect
    {
        public ExtendedMsSql2008Dialect()
        {
            // Tell NH that we can handle the ADO DbTypes
            RegisterColumnType(DbType.UInt16, "INT");
            RegisterColumnType(DbType.UInt32, "BIGINT");
            RegisterColumnType(DbType.UInt64, "DECIMAL(28)");
        }
    }
}
