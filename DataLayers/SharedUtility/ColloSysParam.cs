﻿using System.Text;
using System.Threading.Tasks;

namespace DataLayers.SharedUtility
{
    public class ColloSysParam : BaseColloSysParam
    {
        private static ColloSysParam _webParams;

        public static ColloSysParam WebParams
        {
            get { return _webParams ?? (_webParams = new ColloSysParam()); }
        }
    }
}
