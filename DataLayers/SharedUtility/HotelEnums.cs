﻿using System;

namespace DataLayers.SharedUtility
{
    public static class HotelEnums
    {
        [Serializable]
        public enum UserType
        {
            HotelManager,
            Owner,
            Admin
        }


        [Serializable]
        public enum BookingStatus
        {
            Booked,
            Pending, 
            Confirmed,
            Canceled,
            CheckIn,
            CheckOut,
            Avialable,


        }
        public enum ApplicationMode
        {
            Release,
            Testing,
            Debug
        }

    }
}
