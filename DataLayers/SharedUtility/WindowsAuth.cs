﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace DataLayers.SharedUtility
{
    internal static class WindowsAuth
    {
        public static string GetLoggedInUserName()
        {
            var windowsIdentity = WindowsIdentity.GetCurrent();
            if (windowsIdentity != null)
            {
                var name = windowsIdentity.Name;
                var stop = name.IndexOf("\\", StringComparison.Ordinal);
                return (stop > -1) ? name.Substring(stop + 1, name.Length - stop - 1) : string.Empty;
            }

            return string.Empty;
        }

    }
}
