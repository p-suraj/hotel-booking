﻿using DataLayer;
using DataLayers.SharedUtility;

namespace DataLayers.Domain
{
   public class Users : Entity
    {
       public virtual string Name { get; set; }
       public virtual HotelEnums.UserType UserType { get; set; }
       public virtual double ContactNo { get; set; }
       public virtual string EmailId { get; set; }
       public virtual Hotel Hotel { get; set; }
       public virtual string UserName { get; set; }
       public virtual string Password { get; set; }
    }
}
