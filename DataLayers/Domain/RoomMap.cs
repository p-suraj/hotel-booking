﻿using DataLayer;

namespace DataLayers.Domain
{
   public class RoomMap:EntityMap<Room>
    {
        public RoomMap()
        {
            Property(x=>x.RoomNumber);
            Property(x=>x.RoomTariff);
            ManyToOne(x=>x.RoomType);
           
            Bag(x=>x.RoomRelations, map => { },map=>map.OneToMany());
        }
    }
}
