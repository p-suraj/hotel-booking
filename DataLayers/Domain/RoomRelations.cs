﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using DataLayers.SharedUtility;
using NHibernate.Criterion;

namespace DataLayers.Domain
{
   public  class RoomRelations:Entity
    {
       public virtual Booking  Booking { get; set; }
       public virtual Room Room { get; set; }
       public virtual HotelEnums.BookingStatus Status { get; set; }

    }
}
