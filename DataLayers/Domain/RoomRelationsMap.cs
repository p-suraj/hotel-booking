﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using NHibernate.Mapping;

namespace DataLayers.Domain
{
    public class RoomRelationsMap: EntityMap<RoomRelations>
    {
        public RoomRelationsMap()
        {
            ManyToOne(x => x.Booking, map => map.NotNullable(true));
            ManyToOne(x => x.Room, map => map.NotNullable(true));
            Property(map=>map.Status);
        }
    }
}
