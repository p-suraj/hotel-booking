﻿using System;
using System.Collections.Generic;
using System.Web.Hosting;
using DataLayers.Domain;
using DataLayers.SharedUtility;

namespace DataLayer
{
    public class Booking : Entity
    {
        public virtual string CustomerName { get; set; }
        public virtual string MobileNumber { get; set; }
        public virtual string City { get; set; }
        public virtual string Comment { get; set; }
        public virtual DateTime CheckInDate { get; set; }
        public virtual DateTime CheckOutDate { get; set; }
        public virtual HotelEnums.BookingStatus Status { get; set; }
        public virtual DateTime UpdatetedOn { get; set; }
        public virtual Hotel Hotel { get; set; }
        public virtual IList<RoomRelations> RoomRelations { get; set; }
       }
}
