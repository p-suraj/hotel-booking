﻿using DataLayer;

namespace DataLayers.Domain
{
  public  class UsersMap:EntityMap<Users>
  {
        public UsersMap()
        {
            Property(x=>x.Name);
            Property(x=>x.ContactNo);
            Property(x=>x.EmailId);
            Property(x=>x.UserType);
            Property(x=>x.UserName);
            Property(x=>x.Password);
            ManyToOne(x => x.Hotel);
        }

    }
}
