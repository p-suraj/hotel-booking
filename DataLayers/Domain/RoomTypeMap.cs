﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace DataLayer
{
   public class RoomTypeMap : EntityMap<RoomType>
    {
      public  RoomTypeMap()
        {
            Property(x=>x.RoomCategory);
            Property(x=>x.BedType);
            ManyToOne(x=>x.Hotel,map=>map.NotNullable(true));
            Bag(x=>x.Rooms, map => { },colmap=>colmap.OneToMany());
        }
    }
}
