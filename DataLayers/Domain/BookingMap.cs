﻿using DataLayer;

namespace DataLayers.Domain
{
    public class BookingMap : EntityMap<Booking>
    {
        public BookingMap()
        {
            Property(x => x.CustomerName);
            Property(x => x.City);
            Property(x => x.CheckOutDate, map => map.NotNullable(false));
            Property(x => x.UpdatetedOn, map => map.NotNullable(false));
            Property(x => x.CheckInDate);
            Property(x => x.MobileNumber);
            Property(x => x.Status, map => map.NotNullable(false));
            Property(x=>x.Comment);
            ManyToOne(x => x.Hotel, map => { });

            
            Bag(x=>x.RoomRelations, x => { },map=>map.OneToMany());
        }
    }
}
