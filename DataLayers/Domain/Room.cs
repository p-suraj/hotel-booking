﻿using System.Collections.Generic;
using DataLayer;

namespace DataLayers.Domain
{
    public class Room :Entity
    {
        public virtual uint RoomNumber { get; set; }
        public virtual RoomType RoomType { get; set; }
        public virtual uint? RoomTariff { get; set; }
      
        public virtual IList<RoomRelations> RoomRelations  { get; set; }
        
    }
}
